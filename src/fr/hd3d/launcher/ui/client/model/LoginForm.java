package fr.hd3d.launcher.ui.client.model;

import com.extjs.gxt.ui.client.data.BaseModelData;

import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * Login form is used to carry informations to the controller through the event dispatcher.
 * 
 *@author HD3D
 */
public class LoginForm extends BaseModelData
{
    private static final long serialVersionUID = -4658811177962810261L;

    /** User id field **/
    public final static String USER_ID_FIELD = "userId";
    /** User name text field value */
    public final static String USER_NAME_FIELD = "userName";
    /** Password text field value */
    public final static String PASSWORD_FIELD = "password";
    /** Selected application via combo box */
    public final static String APPLICATION_FIELD = "application";
    /** Remember me option */
    public final static String REMEMBER_ME_FIELD = "rememberMe";

    /**
     * Default constructor, initialize remember field me at false.
     */
    public LoginForm()
    {
        this.set(REMEMBER_ME_FIELD, false);
    }

    public LoginForm(String user, String password, ApplicationModelData application, boolean rememberMe)
    {
        this.set(USER_NAME_FIELD, user);
        this.set(PASSWORD_FIELD, password);
        this.set(APPLICATION_FIELD, application);
        this.set(REMEMBER_ME_FIELD, rememberMe);
    }

    /** @return user name field */
    public String getUserName()
    {
        return this.get(USER_NAME_FIELD);
    }

    public void setUserName(String userName)
    {
        this.set(USER_NAME_FIELD, userName);
    }

    /** @return password field */
    public String getPassword()
    {
        return this.get(PASSWORD_FIELD);
    }

    public void setPassword(String password)
    {
        this.set(PASSWORD_FIELD, password);
    }

    /** @return selected application */
    public ApplicationModelData getApplication()
    {
        return this.get(APPLICATION_FIELD);
    }

    public void setApplication(ApplicationModelData application)
    {
        this.set(APPLICATION_FIELD, application);
    }
    
    public void setUserId(Long userId)
    {
        this.set(USER_ID_FIELD, userId);
    }
    
    public Long getUserId()
    {
        return this.get(USER_ID_FIELD);
    }

    /** @return Remember me option */
    public Boolean getRememberMe()
    {
        if (this.get(REMEMBER_ME_FIELD) == null)
        {
            return false;
        }
        return this.get(REMEMBER_ME_FIELD);
    }

    public void setRememberMe(boolean rememberMe)
    {
        this.set(REMEMBER_ME_FIELD, new Boolean(rememberMe));
    }
}
