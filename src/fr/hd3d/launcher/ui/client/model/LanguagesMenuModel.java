package fr.hd3d.launcher.ui.client.model;

import java.util.ArrayList;

import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.http.HttpRequestHandlerSingleton;
import fr.hd3d.common.ui.client.http.IHttpRequestHandler;
import fr.hd3d.launcher.ui.client.error.LauncherErrors;
import fr.hd3d.launcher.ui.client.model.callback.InitLanguagesCallback;
import fr.hd3d.launcher.ui.client.model.modeldata.LanguageModelData;


/**
 * Handle language menu data.
 * 
 * @author HD3D
 */
public class LanguagesMenuModel
{
    /** Group needed by the menu to identify items which are in the language radio menu. */
    public final static String GROUP_LANGUAGES = "lang";
    /** Default locale, if none is specified. */
    public final static String LOCALE_DEFAULT = "en";
    /** String to give to URL to change language */
    public final static String LOCALE_ARG = "?locale=";

    /** Actually used locale. */
    private String locale;
    /** Languages available. */
    private final ArrayList<LanguageModelData> languages = new ArrayList<LanguageModelData>();

    /** Applications configuration file path */
    final private String CONFIG_FILE_PATH = "config/languages.xml";

    /**
     * Default constructor.
     */
    public LanguagesMenuModel()
    {}

    /** Set actually used locale. */
    public void setLocale(String locale)
    {
        this.locale = locale;
    }

    /** Get actually used locale. */
    public String getLocale()
    {
        if (this.locale == null)
        {
            this.locale = LOCALE_DEFAULT;
        }

        return this.locale;
    }

    /**
     * List used to link a language name to a locale parameter (ex : french -> fr).
     */
    public final ArrayList<LanguageModelData> getLanguages()
    {
        return this.languages;
    }

    /**
     * Map used to link a language name to a locale parameter (ex : french -> fr).
     */
    public final void setLanguages(ArrayList<LanguageModelData> languages)
    {
        this.languages.clear();
        this.languages.addAll(languages);
    }

    /** Get selected language */
    public String getLanguage()
    {
        for (LanguageModelData language : this.languages)
        {
            if (language.getLocale().compareTo(this.getLocale()) == 0)
            {
                return language.getName();
            }
        }

        return null;
    }

    /**
     * Get the locale corresponding to <i>languageName</i>
     * 
     * @return locale corresponding to <i>languageName</i>
     */
    public String getLocale(String languageName)
    {
        for (LanguageModelData language : this.languages)
        {
            if (language.getName().compareTo(languageName) == 0)
            {
                return language.getLocale();
            }
        }

        return null;
    }

    /**
     * Retrieve all languages from configuration file <i>config/languages.xml</i>.
     */
    public void initLanguages()
    {
        final IHttpRequestHandler requestHandler = HttpRequestHandlerSingleton.getInstance();

        try
        {
            requestHandler.getRequest(this.CONFIG_FILE_PATH, new InitLanguagesCallback(this.languages));
        }
        catch (Exception e)
        {
            ErrorDispatcher.sendError(LauncherErrors.LANGUAGES_LOAD);
        }
    }
}
