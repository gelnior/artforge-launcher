package fr.hd3d.launcher.ui.client.model.util.sort;

import java.util.Comparator;

import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * Give ascending sort rules for application model data. The sorting is based on the application name.
 * 
 * @author HD3D
 */
public class ApplicationAscSortRule implements Comparator<ApplicationModelData>
{
    public int compare(ApplicationModelData arg0, ApplicationModelData arg1)
    {
        return arg0.getName().compareTo(arg1.getName());
    }
}
