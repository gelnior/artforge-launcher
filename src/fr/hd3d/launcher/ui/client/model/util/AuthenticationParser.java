package fr.hd3d.launcher.ui.client.model.util;

import org.restlet.client.Response;

import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;

import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.launcher.ui.client.error.LauncherErrors;


/**
 * Analyze service login response.
 * 
 * @author HD3D
 */
public class AuthenticationParser implements IAuthenticationParser
{
    /** The json return by the services after an authentication request. */
    private JSONObject serverResponse;

    /**
     * Tests if result field is same as authentication success value (i.e. "ok").
     * 
     * @param response
     *            REST Response from login web service.
     * @return True if the authentication succeed, else either.
     */
    public Long analyseAuthenticationResponse(Response response)
    {
        Long id = -1L;
        try
        {
            if (response.getEntity() != null && response.getEntity().getText() != null)
            {
                serverResponse = (JSONObject) JSONParser.parse(response.getEntity().getText());

                if (serverResponse.get(RESULT) != null)
                {
                    JSONString result = (JSONString) serverResponse.get(RESULT);
                    if (result.stringValue().compareTo(AUTHENTICATION_SUCCESS_VALUE) == 0)
                    {
                        JSONNumber userId = (JSONNumber) serverResponse.get(USER_ID);
                        id = ((Double) userId.doubleValue()).longValue();
                    }
                }
            }
        }

        catch (Exception e)
        {
            ErrorDispatcher.sendError(LauncherErrors.INVALID_SERVER_RESPONSE);
        }
        return id;
    }
}
