package fr.hd3d.launcher.ui.client.model.util.sort;

import java.util.Comparator;

import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * Give descending sort rules for application model data. The sorting is based on the application name.
 * 
 * @author HD3D
 */
public class ApplicationDescSortRule implements Comparator<ApplicationModelData>
{
    public int compare(ApplicationModelData arg0, ApplicationModelData arg1)
    {
        return arg1.getName().compareTo(arg0.getName());
    }
}
