package fr.hd3d.launcher.ui.client.model.util;

import org.restlet.client.Response;


/**
 * Interface for authentication response parser.
 * 
 * @author HD3D
 */
public interface IAuthenticationParser
{
    /** Result field name of the JSON given by the authentication service. */
    final String RESULT = "result";
    /** Result value corresponding to a success. */
    final String AUTHENTICATION_SUCCESS_VALUE = "ok";
    /** User id field name of the JSON given by the authentication service. */
    final String USER_ID = "userId";

    /**
     * Tests if result field is same as authentication success value (i.e. "ok").
     * 
     * @param response
     *            REST Response from login web service.
     * @return the connect user id.
     */
    public Long analyseAuthenticationResponse(Response response);
}
