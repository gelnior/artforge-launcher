package fr.hd3d.launcher.ui.client.model.util.sort;

import java.util.Comparator;

import fr.hd3d.launcher.ui.client.model.modeldata.LanguageModelData;


/**
 * Give ascending sort rules for language model data. The sorting is based on the language name.
 * 
 * @author HD3D
 */
public class LanguageSortRule implements Comparator<LanguageModelData>
{
    public int compare(LanguageModelData arg0, LanguageModelData arg1)
    {
        return arg0.getName().compareTo(arg1.getName());
    }
}
