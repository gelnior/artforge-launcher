package fr.hd3d.launcher.ui.client.model.util;

/**
 * Singleton that give acces to the authentication parser from any class. It allows to change the parser by a mock one.
 * 
 * @author HD3D
 */
public class AuthenticationParserSingleton
{
    /** The request handler instance to return. */
    private static IAuthenticationParser parser;

    /**
     * @return The set request handler instance. If it is null, it returns a new real REST request handler.
     */
    public final static IAuthenticationParser getInstance()
    {
        if (parser == null)
        {
            parser = new AuthenticationParser();
        }
        return parser;
    }

    /**
     * Set the instance
     * 
     * @param mock
     *            The request handler to set.
     */
    public final static void setInstanceAsMock(IAuthenticationParser mock)
    {
        parser = mock;
    }
}
