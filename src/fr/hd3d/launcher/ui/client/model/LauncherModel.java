package fr.hd3d.launcher.ui.client.model;

import java.util.ArrayList;

import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.http.HttpRequestHandlerSingleton;
import fr.hd3d.common.ui.client.http.IHttpRequestHandler;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.launcher.ui.client.error.LauncherErrors;
import fr.hd3d.launcher.ui.client.model.callback.InitApplicationsCallback;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * Handles launcher data.
 * 
 * @author HD3D
 */
public class LauncherModel extends MainModel
{
    /** Applications configuration file path */
    private final String APPLICATIONS_CONFIG_FILE_PATH = "config/applications.xml";

    /** Application arrayList */
    ArrayList<ApplicationModelData> applications = new ArrayList<ApplicationModelData>();

    /** Language locale that is actually set for all applications. */
    private String locale;

    /**
     * Set language locale
     * 
     * @param locale
     */
    public void setLocale(String locale)
    {
        this.locale = locale;
    }

    /** @return language locale that is actually set for all applications. */
    public String getLocale()
    {
        if (locale == null)
        {
            this.locale = LanguagesMenuModel.LOCALE_DEFAULT;
        }
        return this.locale;
    }

    public ArrayList<ApplicationModelData> getApplications()
    {
        return this.applications;
    }

    /**
     * Retrieve applications data from the applications.xml configuration file.
     */
    public void initApplications()
    {
        try
        {
            IHttpRequestHandler requestHandler = HttpRequestHandlerSingleton.getInstance();
            requestHandler.getRequest(this.APPLICATIONS_CONFIG_FILE_PATH, new InitApplicationsCallback(
                    this.applications));
        }
        catch (Exception e)
        {
            ErrorDispatcher.sendError(LauncherErrors.APPLICATIONS_LOAD);
        }
    }

    /**
     * Retrieves locale parameter form URL.
     */
    public void initLocale(String locale)
    {
        if (locale == null)
        {
            this.locale = LanguagesMenuModel.LOCALE_DEFAULT;
        }
        else
        {
            this.locale = locale;
        }
    }

    /**
     * @param applicationName
     *            The name of the application which should be retrieved.
     * @return Application whom the name is the same as the one given in parameter. Return null if application does not
     *         exist.
     */
    public ApplicationModelData getApplication(String applicationName)
    {
        for (ApplicationModelData application : applications)
        {
            if (application.getName() != null && applicationName.compareTo(application.getName().toLowerCase()) == 0)
            {
                return application;
            }
        }

        return null;
    }
}
