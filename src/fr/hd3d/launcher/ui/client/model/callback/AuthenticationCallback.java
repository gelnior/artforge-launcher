package fr.hd3d.launcher.ui.client.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.error.LauncherErrors;
import fr.hd3d.launcher.ui.client.model.LoginForm;
import fr.hd3d.launcher.ui.client.model.util.AuthenticationParserSingleton;
import fr.hd3d.launcher.ui.client.model.util.IAuthenticationParser;


/**
 * Callback that checks the response from the login web service. If the authentication failed, it raises an error. Else
 * it launches the LOGIN event.
 * 
 * @author HD3D
 */
public class AuthenticationCallback extends BaseCallback
{

    /** The login form to transmit if authentication succeed. */
    LoginForm loginForm;

    public AuthenticationCallback(LoginForm loginForm)
    {
        this.loginForm = loginForm;
    }

    /**
     * If response returned by login web service says result is equal to "ok", it rises a login event with login form
     * information. Otherwise, it sends an error.
     */
    @Override
    protected void onSuccess(Request request, Response response)
    {
        IAuthenticationParser parser = AuthenticationParserSingleton.getInstance();
        Long userId = parser.analyseAuthenticationResponse(response);
        if (userId > -1)
        {
            this.loginForm.setUserId(userId);
            this.dispatchLoginEvent();
        }
        else
        {
            ErrorDispatcher.sendError(LauncherErrors.INVALID_CREDENTIALS);
        }
    }

    /**
     * Send Login Event with login form data to Event Dispatcher.
     */
    private void dispatchLoginEvent()
    {
        AppEvent loginEvent = new AppEvent(LauncherEvents.IS_LOGGING);
        loginEvent.setData(LauncherConfig.EVENT_VAR_LOGIN_FORM, this.loginForm);
        EventDispatcher.forwardEvent(loginEvent);
    }

}
