package fr.hd3d.launcher.ui.client.model.callback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.xml.client.impl.DOMParseException;

import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.error.LauncherErrors;
import fr.hd3d.launcher.ui.client.model.modeldata.LanguageModelData;
import fr.hd3d.launcher.ui.client.model.modeldata.reader.LanguageReader;
import fr.hd3d.launcher.ui.client.model.util.sort.LanguageSortRule;


public class InitLanguagesCallback implements RequestCallback
{
    private final ArrayList<LanguageModelData> languages;

    /** Reader used to parse the XML application configuration file */
    final private LanguageReader reader = new LanguageReader();

    public InitLanguagesCallback(ArrayList<LanguageModelData> languages)
    {
        this.languages = languages;
    }

    public void onError(Request request, Throwable exception)
    {
        ErrorDispatcher.sendError(LauncherErrors.LANGUAGES_LOAD);
    }

    public void onResponseReceived(Request request, Response response)
    {
        if (200 == response.getStatusCode())
        {
            this.processResponse(response);
        }
        else
        {
            ErrorDispatcher.sendError(LauncherErrors.LANGUAGES_LOAD);
        }
    }

    /**
     * Process response, fill the language list with response data.
     * 
     * @param response
     */
    private void processResponse(Response response)
    {
        try
        {
            List<LanguageModelData> result = reader.read(new LanguageModelData(), response.getText());
            languages.addAll(result);
            Collections.sort(languages, new LanguageSortRule());

            EventDispatcher.forwardEvent(LauncherEvents.LANGUAGES_DATA_LOADED);
        }
        catch (DOMParseException e)
        {
            ErrorDispatcher.sendError(LauncherErrors.APPLICATIONS_LOAD);
        }
    }
}
