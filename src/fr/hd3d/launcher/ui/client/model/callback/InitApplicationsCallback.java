package fr.hd3d.launcher.ui.client.model.callback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.xml.client.impl.DOMParseException;

import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.event.LoadApplicationsEvent;
import fr.hd3d.launcher.ui.client.error.LauncherErrors;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.client.model.modeldata.reader.ApplicationReader;
import fr.hd3d.launcher.ui.client.model.util.sort.ApplicationAscSortRule;


/**
 * Callback needed to retrieve data from applications configuration file.
 * 
 * @author HD3D
 */
public class InitApplicationsCallback implements RequestCallback
{
    /** Data list to fill with configuration file data. */
    private final ArrayList<ApplicationModelData> applications;

    /** GXT reader for language model data. */
    final private ApplicationReader reader = new ApplicationReader();

    /**
     * Default constructor.
     * 
     * @param applications
     */
    public InitApplicationsCallback(ArrayList<ApplicationModelData> applications)
    {
        this.applications = applications;
    }

    public void onError(Request request, Throwable exception)
    {
        ErrorDispatcher.sendError(LauncherErrors.APPLICATIONS_LOAD);
    }

    public void onResponseReceived(Request request, Response response)
    {
        if (200 == response.getStatusCode())
        {
            this.processResponse(response);
        }
        else
        {
            ErrorDispatcher.sendError(LauncherErrors.APPLICATIONS_LOAD);
        }
    }

    /**
     * Process response, fill the application list with response data.
     * 
     * @param response
     */
    private void processResponse(Response response)
    {
        try
        {
            List<ApplicationModelData> result = reader.read(new ApplicationModelData(), response.getText());

            this.applications.addAll(result);
            Collections.sort(this.applications, new ApplicationAscSortRule());

            this.dispatchLoadedEvent();
        }
        catch (DOMParseException e)
        {
            ErrorDispatcher.sendError(LauncherErrors.APPLICATIONS_LOAD);
        }
    }

    /** Dispatch loading success event to controller (with applications list). */
    private void dispatchLoadedEvent()
    {
        LoadApplicationsEvent event = new LoadApplicationsEvent();
        event.setData(LauncherConfig.APPLICATIONS_EVENT_VAR_NAME, this.applications);
        EventDispatcher.forwardEvent(event);
    }
}
