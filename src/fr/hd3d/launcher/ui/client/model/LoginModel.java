package fr.hd3d.launcher.ui.client.model;

import java.util.List;

import org.restlet.client.data.Form;
import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.callback.EventCallback;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.model.callback.AuthenticationCallback;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * Handles login form data.
 * 
 * @author HD3D
 */
public class LoginModel
{

    LoginForm loginForm = new LoginForm();

    /** Store used by the application combo box */
    private ListStore<ApplicationModelData> appStore = new ListStore<ApplicationModelData>();;

    /**
     * Fill data store with application list given in parameter.
     * 
     * @param applications
     */
    public void fillStore(List<ApplicationModelData> applications)
    {
        this.appStore.add(applications);
    }

    /**
     * @return Application store used by the combo box.
     */
    public ListStore<ApplicationModelData> getComboStore()
    {
        return this.appStore;
    }

    /**
     * Set application store used by the combo box.
     */
    public void setStore(ListStore<ApplicationModelData> appStore)
    {
        this.appStore = appStore;
    }

    /**
     * @return Object that contains login form values.
     */
    public LoginForm getLoginForm()
    {
        return this.loginForm;
    }

    /**
     * @param applicationName
     *            The name of the application which should be retrieved.
     * @return Application whom the name is the same as the one given in parameter. Return null if application does not
     *         exist.
     */
    public ApplicationModelData getApplication(String applicationName)
    {
        for (ApplicationModelData application : this.getComboStore().getModels())
        {
            if (application.getName() != null
                    && applicationName.toLowerCase().compareTo(application.getName().toLowerCase()) == 0)
            {
                return application;
            }
        }

        return null;
    }

    /**
     * Verify credentials passed via the login dialog by calling the login web service.
     * 
     * @param loginForm
     *            Login parameters (user name and password).
     */
    public void checkLogin()
    {
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        Form form = new Form();

        form.add(LauncherConfig.LOGIN_FORM_LOGIN, this.loginForm.getUserName());
        form.add(LauncherConfig.LOGIN_FORM_PASSWORD, this.loginForm.getPassword());

        if (this.loginForm.getRememberMe())
        {
            form.add(LauncherConfig.LOGIN_FORM_REMEMBER, LauncherConfig.LOGIN_FORM_REMEMBER_ON);
        }

        requestHandler.handleAttributeRequest(Method.POST, LauncherConfig.LOGIN_PATH, form, new AuthenticationCallback(
                loginForm));
    }

    /** Send a log out request to server. */
    public void logOut()
    {
        try
        {
            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            requestHandler.getRequest(LauncherConfig.LOGOUT_PATH, new EventCallback(LauncherEvents.LOG_OUT));
        }
        catch (Exception e)
        {
            ErrorDispatcher.sendError(CommonErrors.NO_SERVICE_CONNECTION);
        }
    }
}
