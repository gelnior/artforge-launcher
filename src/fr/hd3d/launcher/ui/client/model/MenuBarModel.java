package fr.hd3d.launcher.ui.client.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.client.model.util.sort.ApplicationAscSortRule;
import fr.hd3d.launcher.ui.client.model.util.sort.ApplicationDescSortRule;


/**
 * Handles menu bar data.
 * 
 * @author HD3D
 */
public class MenuBarModel
{
    /** Symbol CSS class name. */
    public final static String SYMBOL_STYLE = "menu-bar-symbol";
    /** Main application label style name */
    public static final String MAIN_APP_STYLE = "main-app-label";

    /** Menu bar height in pixel. */
    public final static String MENU_BAR_HEIGHT = "25px";

    /** Name given to online help window */
    public static final String HELP_WINDOW_NAME = "help_window";
    /**
     * Settings for new window opening, this sets all stuff : menu bar, directories, location, tool bar, scroll bars,
     * status bar and resizable.
     */
    public static final String NEW_WINDOW_SETTINGS = "menubar=yes,directories=yes,location=yes,resizable=yes,toolbar=yes,scrollbars=yes,status=yes";

    /** Running application. */
    private ApplicationModelData mainApp;
    /** Available applications. */
    private List<ApplicationModelData> appsList = new ArrayList<ApplicationModelData>();
    /** Applications from the same family as the running application. */
    private final ArrayList<ApplicationModelData> familyAppsList = new ArrayList<ApplicationModelData>();
    /** Applications from other family than the running application. */
    private final ArrayList<ApplicationModelData> foreignAppsList = new ArrayList<ApplicationModelData>();

    /** Current user name. */
    private String userName;

    /** Default constructor. */
    public MenuBarModel()
    {}

    /**
     * Set available applications
     * 
     * @param applications
     *            available applications
     */
    public void setApplications(List<ApplicationModelData> applications)
    {
        if (applications != null)
        {
            this.appsList = applications;
        }
    }

    /**
     * @return available application list.
     */
    public List<ApplicationModelData> getApplications()
    {
        return this.appsList;
    }

    /**
     * @return current user name.
     */
    public String getUserName()
    {
        return this.userName;
    }

    /**
     * Set current user name.
     * 
     * @param userName
     *            User name to set.
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    /**
     * @return running application.
     */
    public ApplicationModelData getMainApp()
    {
        return this.mainApp;
    }

    /**
     * Set running application.
     * 
     * @param app
     *            application to set.
     */
    public void setMainApp(ApplicationModelData app)
    {
        this.mainApp = app;
        this.orderApps();
    }

    /**
     * Get applications from the same family as the running application family.
     * 
     * @return applications from the same family as the running application family.
     */
    public ArrayList<ApplicationModelData> getFamilyApps()
    {
        return this.familyAppsList;
    }

    /**
     * Get applications from other families than the running application family.
     * 
     * @return applications from the other families than the running application family.
     */
    public ArrayList<ApplicationModelData> getForeignApps()
    {
        return this.foreignAppsList;
    }

    /**
     * @return running application about text.
     */
    public String getAboutText()
    {
        return this.mainApp.getAboutText();
    }

    /**
     * @return running application online help URL.
     */
    public String helpUrl()
    {
        return this.mainApp.getHelpUrl();
    }

    /**
     * Reset model data with data coming from the login form.
     * 
     * @param form
     */
    public void processLoginForm(LoginForm form)
    {
        this.userName = form.getUserName();
        this.setMainApp(form.getApplication());
    }

    /**
     * Refresh application family and foreign lists. Foreign is sorted ascending because of the GXT insertion mode in
     * menu which puts next inserted item over the previous one.
     */
    private void orderApps()
    {
        familyAppsList.clear();
        foreignAppsList.clear();

        String familyApp = this.mainApp.getFamily();

        for (ApplicationModelData app : appsList)
        {
            if (app.getFamily().compareTo(familyApp) == 0)
            {
                this.familyAppsList.add(app);
            }
            else
            {
                this.foreignAppsList.add(app);
            }
        }

        Collections.sort(this.familyAppsList, new ApplicationDescSortRule());
        Collections.sort(this.foreignAppsList, new ApplicationAscSortRule());
    }
}
