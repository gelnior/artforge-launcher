package fr.hd3d.launcher.ui.client.model.modeldata.reader;

import java.util.List;

import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * Reader is used to parse XML file containing application objects.
 * 
 * @author HD3D
 */
public class ApplicationReader extends Hd3dXmlReader<ApplicationModelData>
{
    /**
     * Default constructor. Set automatically model type to application model type.
     */
    public ApplicationReader()
    {
        super(ApplicationModelData.getModelType());
    }

    @Override
    public List<ApplicationModelData> read(Object loadConfig, Object data)
    {
        return super.read(loadConfig, data);
    }

    @Override
    protected ApplicationModelData newModelInstance()
    {
        return new ApplicationModelData();
    }
}
