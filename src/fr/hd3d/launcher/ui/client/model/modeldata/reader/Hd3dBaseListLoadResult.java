package fr.hd3d.launcher.ui.client.model.modeldata.reader;

import java.io.Serializable;
import java.util.List;

import com.extjs.gxt.ui.client.data.ListLoadResult;


/**
 * BaseListLoadResult class that don't raise warnings.
 * 
 * @author HD3D
 */
public class Hd3dBaseListLoadResult<C> implements ListLoadResult<C>, Serializable
{
    /**
     * Version ID needed by serializable interface.
     */
    private static final long serialVersionUID = -7832856404895951952L;

    /**
     * The remote data.
     */
    protected List<C> list;

    Hd3dBaseListLoadResult()
    {

    }

    /**
     * Creates a new list load result.
     * 
     * @param list
     *            the data
     */
    public Hd3dBaseListLoadResult(List<C> list)
    {
        this.list = list;
    }

    public List<C> getData()
    {
        return list;
    }

    public void setData(List<C> list)
    {
        this.list = list;
    }

}
