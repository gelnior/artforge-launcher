package fr.hd3d.launcher.ui.client.model.modeldata.reader;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.DomQuery;
import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.ModelType;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.XMLParser;


/**
 * XML reader class that don't raise warnings.
 * 
 * @author HD3D
 */
public abstract class Hd3dXmlReader<C extends ModelData> implements DataReader<List<C>>
{

    private final ModelType modelType;

    /**
     * Creates a new xml reader instance.
     * 
     * @param modelType
     *            the model type
     */
    public Hd3dXmlReader(ModelType modelType)
    {
        this.modelType = modelType;
    }

    public List<C> read(Object loadConfig, Object data)
    {
        Document doc = XMLParser.parse((String) data);

        NodeList list = doc.getElementsByTagName(modelType.getRecordName());
        ArrayList<C> records = new ArrayList<C>();
        for (int i = 0; i < list.getLength(); i++)
        {
            Node node = list.item(i);
            Element elem = (Element) node;
            C model = newModelInstance();

            for (int j = 0; j < modelType.getFieldCount(); j++)
            {
                DataField field = modelType.getField(j);
                String map = field.getMap() != null ? field.getMap() : field.getName();
                String v = getValue(elem, map);
                model.set(field.getName(), v);
            }
            records.add(model);
        }

        // int totalCount = records.size();

        Node root = doc.getElementsByTagName(modelType.getRoot()).item(0);
        if (root != null && modelType.getTotalName() != null)
        {
            Node totalNode = root.getAttributes().getNamedItem(modelType.getTotalName());
            if (totalNode != null)
            {
                // String sTot = totalNode.getNodeValue();
                // totalCount = Integer.parseInt(sTot);
            }
        }
        // ListLoadResult<C> result = newLoadResult(loadConfig, records);
        List<C> result = new ArrayList<C>(records);

        return result;
    }

    protected native JavaScriptObject getJsObject(Element elem) /*-{
        return elem.@com.google.gwt.xml.client.impl.DOMItem::getJsObject()();
    }-*/;

    protected String getValue(Element elem, String name)
    {
        return DomQuery.selectValue(name, getJsObject(elem));
    }

    /**
     * Template method that provides load result.
     * 
     * @param models
     *            the models
     * @return the load result
     */
    protected ListLoadResult<C> newLoadResult(C loadConfig, List<C> models)
    {
        return new Hd3dBaseListLoadResult<C>(models);
    }

    /**
     * Returns the new model instances. Subclasses may override to provide a model data subclass.
     * 
     * @return the new model data instance
     */
    abstract protected C newModelInstance();
}
