package fr.hd3d.launcher.ui.client.model.modeldata.reader;

import java.util.List;

import fr.hd3d.launcher.ui.client.model.modeldata.LanguageModelData;


/**
 * Reader is used to parse XML file containing language objects.
 * 
 * @author HD3D
 */
public class LanguageReader extends Hd3dXmlReader<LanguageModelData>
{
    /**
     * Default constructor. Set automatically model type to application model type.
     */
    public LanguageReader()
    {
        super(LanguageModelData.getModelType());
    }

    @Override
    public List<LanguageModelData> read(Object loadConfig, Object data)
    {
        return super.read(loadConfig, data);
    }

    @Override
    protected LanguageModelData newModelInstance()
    {
        return new LanguageModelData();
    }
}
