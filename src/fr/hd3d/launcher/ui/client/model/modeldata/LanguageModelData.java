package fr.hd3d.launcher.ui.client.model.modeldata;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.ModelType;


/**
 * Model to handle data about applications to manage
 * 
 * @author HD3D
 */
public class LanguageModelData extends BaseModelData
{
    /** UID used for serialization. */
    private static final long serialVersionUID = 4634466975095245092L;

    /** Application name. */
    public static final String NAME = "name";
    /** Each application has a family. Family are used to improve access to related applications. */
    public static final String LOCALE = "locale";

    /** Root object name for the language configuration file. */
    public static final String ROOT_NAME = "languages";
    /** Record object name for the language configuration file. */
    private static final String RECORD_NAME = "language";

    /**
     * Empty constructor, used by reader to create ApplicationModel and then fill it with retrieved data.
     */
    public LanguageModelData()
    {

    }

    /**
     * Default constructor.
     * 
     * @param name
     *            Language
     * @param family
     *            Locale abbreviation
     */
    public LanguageModelData(String name, String family)
    {
        setName(name);
        setLocale(family);
    }

    public void setName(String name)
    {
        set(LanguageModelData.NAME, name);
    }

    public String getName()
    {
        return get(LanguageModelData.NAME);
    }

    public void setLocale(String family)
    {
        set(LanguageModelData.LOCALE, family);
    }

    public String getLocale()
    {
        return get(LanguageModelData.LOCALE);
    }

    /**
     * @return Model type used by reader to handle the language model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = new ModelType();
        type.setRecordName(LanguageModelData.RECORD_NAME);
        type.setRoot(LanguageModelData.ROOT_NAME);

        type.addField(LanguageModelData.NAME);
        type.addField(LanguageModelData.LOCALE);

        return type;
    }
}
