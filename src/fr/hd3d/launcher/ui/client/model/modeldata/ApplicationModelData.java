package fr.hd3d.launcher.ui.client.model.modeldata;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.ModelType;


/**
 * Model to handle data about applications to manage
 * 
 * @author HD3D
 */
public class ApplicationModelData extends BaseModelData
{
    /** UID used for serialization. */
    private static final long serialVersionUID = 5478606665668420210L;
    /** Application name. */
    public static final String NAME = "name";
    /** Each application has a family. Family are used to improve access to related applications. */
    public static final String FAMILY = "family";
    /** Application URL. */
    public static final String URL = "url";
    /** Application online help URL. */
    public static final String HELP_URL = "helpUrl";
    /** About text to display in the about dialog box. */
    public static final String ABOUT_TEXT = "aboutText";

    /**
     * Empty constructor, used by reader to create ApplicationModel and then fill it with retrieved data.
     */
    public ApplicationModelData()
    {

    }

    /**
     * Default constructor.
     * 
     * @param name
     *            Application Name.
     * @param family
     *            Application family Name.
     * @param url
     *            Application URL.
     * @param helpUrl
     *            Application online help URL.
     * @param aboutText
     *            About text to display in the about dialog box.
     */
    public ApplicationModelData(String name, String family, String url, String helpUrl, String aboutText)
    {
        setName(name);
        setFamily(family);
        setUrl(url);
        setHelpUrl(helpUrl);
        setAboutText(aboutText);
    }

    public void setName(String name)
    {
        set(ApplicationModelData.NAME, name);
    }

    public String getName()
    {
        return get(ApplicationModelData.NAME);
    }

    public void setFamily(String family)
    {
        set(ApplicationModelData.FAMILY, family);
    }

    public String getFamily()
    {
        return get(ApplicationModelData.FAMILY);
    }

    public void setUrl(String url)
    {
        set(ApplicationModelData.URL, url);
    }

    public String getUrl()
    {
        return get(ApplicationModelData.URL);
    }

    public void setHelpUrl(String helpUrl)
    {
        set(ApplicationModelData.HELP_URL, helpUrl);
    }

    public String getHelpUrl()
    {
        return get(ApplicationModelData.HELP_URL);
    }

    public void setAboutText(String aboutText)
    {
        set(ApplicationModelData.ABOUT_TEXT, aboutText);
    }

    public String getAboutText()
    {
        return get(ApplicationModelData.ABOUT_TEXT);
    }

    /**
     * @return Model type used by reader to handle the application model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = new ModelType();
        type.setRecordName("application");
        type.setRoot("applications");

        type.addField(ApplicationModelData.NAME);
        type.addField(ApplicationModelData.FAMILY);
        type.addField(ApplicationModelData.URL);
        type.addField(ApplicationModelData.HELP_URL);
        type.addField(ApplicationModelData.ABOUT_TEXT);

        return type;
    }
}
