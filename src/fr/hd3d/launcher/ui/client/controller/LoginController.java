package fr.hd3d.launcher.ui.client.controller;

import com.extjs.gxt.ui.client.Registry;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.controller.event.LoadApplicationsEvent;
import fr.hd3d.launcher.ui.client.error.LauncherErrors;
import fr.hd3d.launcher.ui.client.model.LoginModel;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.client.view.ILoginDialog;


/**
 * Controller that handles actions on login dialog box.
 * 
 * @author HD3D
 */
public class LoginController extends Controller
{
    /** Model which handle application data for login form. */
    private final LoginModel model;
    /** Login dialog box. */
    private final ILoginDialog view;

    /** Default constructor. */
    public LoginController(LoginModel model, ILoginDialog view)
    {
        this.model = model;
        this.view = view;

        this.registerView();
        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == LauncherEvents.LOGIN_CLICK)
        {
            this.onLoginClick();
        }
        else if (type == LauncherEvents.IS_LOGGING)
        {
            this.onLogIn();
        }
        else if (type == LauncherEvents.APPLICATIONS_DATA_LOADED)
        {
            this.onApplicationDataLoaded(event);
        }
        else if (type == LauncherEvents.LOG_OUT_CONFIRMED)
        {
            this.onLogOutConfirmed();
        }
        else if (type == LauncherEvents.LOG_OUT)
        {
            this.onLogOut();
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When log out is confirmed by user it sends a log out request to server.
     */
    private void onLogOutConfirmed()
    {
        this.model.logOut();
    }

    /**
     * When Log In button is clicked, the controller checks if fields are valid (not empty) then ask for launcher to log
     * in selected application.
     */
    private void onLoginClick()
    {
        String userName = this.view.getUserName();
        String password = this.view.getPassword();
        ApplicationModelData application = this.view.getApplication();
        boolean rememberMe = this.view.getRememberMe();

        boolean testUserName = userName != null && userName.length() > 0;
        boolean testPassword = password != null && password.length() > 0;
        boolean testApplication = application != null;

        if (testUserName && testPassword && testApplication)
        {
            this.view.showLoading();
            this.model.getLoginForm().setUserName(userName);
            this.model.getLoginForm().setPassword(password);
            this.model.getLoginForm().setApplication(application);
            this.model.getLoginForm().setRememberMe(rememberMe);
            this.model.checkLogin();
        }
        else
        {
            ErrorDispatcher.sendError(LauncherErrors.INVALID_LOGIN_FORM);
        }

        this.view.disableListener();
    }

    /**
     * When application data are loaded, the controller fills the combo store with the applications list and
     * automatically select the application given in the URL parameters.
     * 
     * @param event
     */
    private void onApplicationDataLoaded(AppEvent event)
    {
        LoadApplicationsEvent appEvent = (LoadApplicationsEvent) event;
        this.model.fillStore(appEvent.getApplications());

        this.selectApplication();
    }

    /**
     * Automatically select the application given in the URL parameters.
     */
    private void selectApplication()
    {
        String applicationName = this.view.getApplicationParameter();

        if (applicationName == null)
        {
            applicationName = FavoriteCookie.getFavParameterValue(LauncherConfig.COOKIE_VAR_APPLICATION);
        }

        if (applicationName != null)
        {
            ApplicationModelData application = this.model.getApplication(applicationName);

            this.model.getLoginForm().setApplication(application);
            this.view.selectApplication(application);
        }

        if (this.model.getComboStore().getCount() == 1)
        {
            ApplicationModelData application = this.model.getComboStore().getAt(0);
            this.view.selectApplication(application);
            this.view.hideApplicationCombobox();
        }
    }

    /**
     * Reset Fields when dialog box should be shown after a log out event.
     */
    private void onLogOut()
    {
        this.view.reset();
        this.selectApplication();
    }

    /**
     * When login is done, it stops the loading widget.
     */
    private void onLogIn()
    {
        this.view.hideLoading();
        this.view.enableEnterKeyListener();
    }

    /**
     * When error appends, it stops the loading widget.
     * 
     */
    private void onError()
    {
        this.view.hideLoading();
    }

    /** Register dialog box to view registry. */
    private void registerView()
    {
        Registry.register("login_dialog", this.view);
    }

    /** Register all events the controller can handle. */
    private void registerEvents()
    {
        this.registerEventTypes(LauncherEvents.LOGIN_CLICK);
        this.registerEventTypes(LauncherEvents.IS_LOGGING);
        this.registerEventTypes(LauncherEvents.APPLICATIONS_DATA_LOADED);
        this.registerEventTypes(LauncherEvents.LOG_OUT);
        this.registerEventTypes(LauncherEvents.LOG_OUT_CONFIRMED);
        this.registerEventTypes(CommonEvents.ERROR);
    }
}
