package fr.hd3d.launcher.ui.client.controller.event;

import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * Event type used for data loading actions. Event type is always APPLICATIONS_DATA_LOADED.
 * 
 * @author HD3D
 */
public class LoadApplicationsEvent extends AppEvent
{

    /**
     * Default constructor.
     */
    public LoadApplicationsEvent()
    {
        super(LauncherEvents.APPLICATIONS_DATA_LOADED);
    }

    /**
     * @return applications carried by event.
     */
    public List<ApplicationModelData> getApplications()
    {
        return super.getData(LauncherConfig.APPLICATIONS_EVENT_VAR_NAME);
    }

    /**
     * Set a new application list inside the event.
     * 
     * @param applications
     *            The application list to in the event data.
     */

    public void setApplications(List<ApplicationModelData> applications)
    {
        super.setData(LauncherConfig.APPLICATIONS_EVENT_VAR_NAME, applications);
    }
}
