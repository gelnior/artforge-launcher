package fr.hd3d.launcher.ui.client.controller.event;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Give a code to all events raised by the launcher.
 * 
 * @author HD3D
 */
public class LauncherEvents
{
    public static final int START_EVENT = 19000;

    public static final EventType INIT = new EventType(START_EVENT + 100);
    public static final EventType CONFIG_INITIALIZED = new EventType(START_EVENT + 101);

    public static final EventType IS_LOGGING = new EventType(START_EVENT + 110);
    public static final EventType LOG_OUT_CLICKED = new EventType(START_EVENT + 111);
    public static final EventType LOG_OUT = new EventType(START_EVENT + 112);
    public static final EventType LOGIN_CLICK = new EventType(START_EVENT + 113);
    public static final EventType ALREADY_LOGGED = new EventType(START_EVENT + 114);
    public static final EventType LOG_OUT_CONFIRMED = new EventType();

    public static final EventType LAUNCHING = new EventType(START_EVENT + 121);
    public static final EventType APP_CHANGING_CONFIRMATION = new EventType(START_EVENT + 122);
    public static final EventType APP_CHANGED = new EventType(START_EVENT + 123);
    public static final EventType NEW_APPLICATION_WINDOW_CLICKED = new EventType(START_EVENT + 124);

    public static final EventType CHANGE_LANGUAGE_CONFIRMED = new EventType(START_EVENT + 131);
    public static final EventType LANGUAGE_SELECTED = new EventType(START_EVENT + 132);
    public static final EventType KEEP_LANGUAGE = new EventType(START_EVENT + 133);

    public static final EventType GO_WEBSITE = new EventType(START_EVENT + 141);
    public static final EventType GO_ONLINE_HELP_CLICKED = new EventType(START_EVENT + 142);

    public static final EventType ABOUT_CLICKED = new EventType(START_EVENT + 151);

    public static final EventType WINDOW_RESIZE = new EventType(START_EVENT + 161);

    public static final EventType APPLICATIONS_DATA_LOADED = new EventType(START_EVENT + 200);
    public static final EventType LANGUAGES_DATA_LOADED = new EventType(START_EVENT + 201);

}
