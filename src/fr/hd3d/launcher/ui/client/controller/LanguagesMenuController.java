package fr.hd3d.launcher.ui.client.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.model.LanguagesMenuModel;
import fr.hd3d.launcher.ui.client.view.ILanguagesMenu;


/**
 * Languages menu controller. It handles the CHANGE_LANGUAGE and KEEP_LANGUAGE events.
 * 
 * @author HD3D
 */
public class LanguagesMenuController extends Controller
{
    /** Languages Menu widget */
    private final ILanguagesMenu view;
    /** Data model */
    private final LanguagesMenuModel model;

    /**
     * Default constructor.
     */
    public LanguagesMenuController(ILanguagesMenu view, LanguagesMenuModel model)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == CommonEvents.CONFIG_INITIALIZED)
        {
            this.model.setLocale(this.view.getLocaleParameter());
            this.model.initLanguages();
        }
        else if (type == LauncherEvents.CHANGE_LANGUAGE_CONFIRMED)
        {
            this.model.setLocale((String) event.getData(LauncherConfig.EVENT_VAR_LOCALE));
        }
        else if (type == LauncherEvents.KEEP_LANGUAGE)
        {
            this.view.checkLanguage(this.model.getLanguage());
        }
        else if (type == LauncherEvents.LANGUAGES_DATA_LOADED)
        {
            this.view.fill();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /** Register all events the controller can handle. */
    private void registerEvents()
    {
        this.registerEventTypes(LauncherEvents.KEEP_LANGUAGE);
        this.registerEventTypes(LauncherEvents.CONFIG_INITIALIZED);
        this.registerEventTypes(LauncherEvents.CHANGE_LANGUAGE_CONFIRMED);
        this.registerEventTypes(LauncherEvents.LANGUAGES_DATA_LOADED);

        this.registerEventTypes(CommonEvents.CONFIG_INITIALIZED);
    }
}
