package fr.hd3d.launcher.ui.client.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.controller.event.LoadApplicationsEvent;
import fr.hd3d.launcher.ui.client.model.LoginForm;
import fr.hd3d.launcher.ui.client.model.MenuBarModel;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.client.view.IMenuBar;


/**
 * Controller for the launcher menu bar.
 * 
 * @author HD3D
 */
public class MenuBarController extends Controller
{
    /** View to control */
    private final IMenuBar view;
    /** Model to handle data */
    private final MenuBarModel model;

    /**
     * Default constructor.
     */
    public MenuBarController(MenuBarModel model, IMenuBar view)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        String locale;

        EventType type = event.getType();
        if (type == LauncherEvents.APPLICATIONS_DATA_LOADED)
        {
            LoadApplicationsEvent appEvent = (LoadApplicationsEvent) event;
            this.model.setApplications(appEvent.getApplications());
        }
        else if (type == LauncherEvents.ALREADY_LOGGED)
        {
            this.onLogin(event);
        }

        else if (type == LauncherEvents.IS_LOGGING)
        {
            this.onLogin(event);
        }
        else if (type == LauncherEvents.LOG_OUT_CLICKED)
        {
            this.view.displayConfirmLogOut();
        }
        else if (type == LauncherEvents.LAUNCHING)
        {
            this.view.refreshAppMenu();
            this.view.refreshUserName();
        }

        else if (type == LauncherEvents.ABOUT_CLICKED)
        {
            this.view.displayAbout(this.model.getAboutText());
        }
        else if (type == LauncherEvents.GO_ONLINE_HELP_CLICKED)
        {
            this.view.openWebsite(this.model.helpUrl());
        }

        else if (type == LauncherEvents.APP_CHANGING_CONFIRMATION)
        {
            ApplicationModelData app = event.getData(LauncherConfig.EVENT_VAR_APPLICATION);
            this.view.displayConfirmAppChange(app);
        }
        else if (type == LauncherEvents.APP_CHANGED)
        {
            this.onChangeApp(event);
        }
        else if (type == LauncherEvents.NEW_APPLICATION_WINDOW_CLICKED)
        {
            this.onNewApplicationWindow(event);
        }
        else if (type == LauncherEvents.CHANGE_LANGUAGE_CONFIRMED)
        {
            this.forwardToChild(event);
            this.onLanguageChange(event);
        }
        else if (type == LauncherEvents.LANGUAGE_SELECTED)
        {
            locale = (String) event.getData(LauncherConfig.URL_VAR_LOCALE);
            this.view.displayConfirmLanguageChange(locale);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When application is changed it sets the new main app then it forwards the launch event.
     */
    private void onChangeApp(AppEvent event)
    {
        ApplicationModelData app = event.getData(LauncherConfig.EVENT_VAR_APPLICATION);
        this.model.setMainApp(app);

        AppEvent eventlaunch = new AppEvent(LauncherEvents.LAUNCHING);
        eventlaunch.setData(LauncherConfig.EVENT_VAR_APPLICATION, this.model.getMainApp());
        EventDispatcher.forwardEvent(eventlaunch);
    }

    /**
     * When user asks for opening an application in a new window, it opens a new launcher window with application
     * parameter corresponding to the selected application.
     */
    private void onNewApplicationWindow(AppEvent event)
    {
        ApplicationModelData app = event.getData(LauncherConfig.EVENT_VAR_APPLICATION);
        String applicationName = app.getName();

        Map<String, List<String>> parametersMap = this.view.getUrlParameterMap();
        Map<String, List<String>> parametersToSet = new HashMap<String, List<String>>(parametersMap);
        ArrayList<String> applicationList = new ArrayList<String>();
        applicationList.add(applicationName.toLowerCase());

        parametersToSet.put(LauncherConfig.URL_VAR_APPLICATION, applicationList);

        this.view.openLauncher(this.parametersMapToString(parametersToSet), "Window_" + applicationName);
    }

    /**
     * Launch the new application and sets variable such as user name and selected application in the menu bar model.
     * 
     * @param event
     *            The event carrying login parameters.
     */
    private void onLogin(AppEvent event)
    {
        this.model.processLoginForm((LoginForm) event.getData(LauncherConfig.EVENT_VAR_LOGIN_FORM));

        AppEvent eventLaunch = new AppEvent(LauncherEvents.LAUNCHING);
        eventLaunch.setData(LauncherConfig.EVENT_VAR_APPLICATION, this.model.getMainApp());
        EventDispatcher.forwardEvent(eventLaunch);
    }

    /**
     * When language changes, controller retrieves URL parameters, add application and locale parameters and set the
     * navigator URL with the new parameter set.
     * 
     * @param event
     */
    private void onLanguageChange(AppEvent event)
    {
        String locale = (String) event.getData(LauncherConfig.EVENT_VAR_LOCALE);
        String urlParameters = this.getLocaleUrlParameters(locale);

        this.view.changeLanguage(urlParameters);
    }

    /**
     * Get URL parameters and update the locale parameter. If the locale parameter does not exist, locale parameter is
     * created and added to the URL parameters.
     * 
     * @param locale
     *            Locale to set in the parameters
     */
    private String getLocaleUrlParameters(String locale)
    {

        Map<String, List<String>> map = this.view.getUrlParameterMap();
        Map<String, List<String>> parameterMap = new HashMap<String, List<String>>(map);

        ArrayList<String> localeList = new ArrayList<String>();
        localeList.add(locale);

        parameterMap.put(LauncherConfig.URL_VAR_LOCALE, localeList);

        return this.parametersMapToString(parameterMap);
    }

    /**
     * @param parametersMap
     *            the map to convert to string.
     * @return Parameters map into a string format adapted for URL.
     */
    private String parametersMapToString(Map<String, List<String>> parametersMap)
    {
        String urlParameters = new String();
        boolean first = true;
        for (Map.Entry<String, List<String>> entry : parametersMap.entrySet())
        {
            if (!first)
            {
                urlParameters += "&";
            }
            urlParameters += entry.getKey() + "=" + entry.getValue().get(0);
            first = false;
        }

        return urlParameters;
    }

    /** Register all events the controller can handle. */
    private void registerEvents()
    {
        this.registerEventTypes(LauncherEvents.ABOUT_CLICKED);
        this.registerEventTypes(LauncherEvents.LOG_OUT_CLICKED);
        this.registerEventTypes(LauncherEvents.ALREADY_LOGGED);
        this.registerEventTypes(LauncherEvents.APPLICATIONS_DATA_LOADED);
        this.registerEventTypes(LauncherEvents.GO_WEBSITE);
        this.registerEventTypes(LauncherEvents.GO_ONLINE_HELP_CLICKED);
        this.registerEventTypes(LauncherEvents.IS_LOGGING);
        this.registerEventTypes(LauncherEvents.LAUNCHING);
        this.registerEventTypes(LauncherEvents.LOG_OUT);
        this.registerEventTypes(LauncherEvents.CHANGE_LANGUAGE_CONFIRMED);
        this.registerEventTypes(LauncherEvents.APP_CHANGING_CONFIRMATION);
        this.registerEventTypes(LauncherEvents.APP_CHANGED);
        this.registerEventTypes(LauncherEvents.NEW_APPLICATION_WINDOW_CLICKED);
        this.registerEventTypes(LauncherEvents.LANGUAGE_SELECTED);
        this.registerEventTypes(CommonEvents.CONFIG_INITIALIZED);
    }
}
