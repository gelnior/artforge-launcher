package fr.hd3d.launcher.ui.client.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.widget.mainview.MainController;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.error.LauncherErrors;
import fr.hd3d.launcher.ui.client.model.LanguagesMenuModel;
import fr.hd3d.launcher.ui.client.model.LauncherModel;
import fr.hd3d.launcher.ui.client.model.LoginForm;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.client.view.ILauncher;


/**
 * Main controller that handles login and application launch events.
 * 
 * @author HD3D
 */
public class LauncherController extends MainController
{
    /** Launcher widget view */
    final private ILauncher view;

    /** Data model */
    final private LauncherModel model;

    /** Default constructor. */
    public LauncherController(LauncherModel model, ILauncher view)
    {
        super(model, view);

        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);

        EventType type = event.getType();

        if (type == LauncherEvents.LANGUAGES_DATA_LOADED)
        {
            this.forwardToChild(event);
            this.model.initApplications();
        }
        else if (type == LauncherEvents.APPLICATIONS_DATA_LOADED)
        {
            this.forwardToChild(event);
            this.onApplicationDataLoaded();
        }

        else if (type == LauncherEvents.IS_LOGGING)
        {
            this.onLogin(event);
            this.forwardToChild(event);
        }
        else if (type == LauncherEvents.LAUNCHING)
        {
            this.onLaunch(event);
            this.forwardToChild(event);
        }
        else if (type == LauncherEvents.LOG_OUT)
        {
            this.forwardToChild(event);
            this.onLogOut();
        }

        else if (type == LauncherEvents.WINDOW_RESIZE)
        {
            this.view.centerLoginDialog();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When service configuration is done, it asks for model to initialize applications data.
     */
    @Override
    protected void onConfigInitialized(AppEvent event)
    {
        this.model.initLocale(this.view.getLocaleParameter());
    }

    /**
     * When application are loaded, if the authentication cookie is set, it logs user automatically. If not, it displays
     * the login dialog box.
     */
    private void onApplicationDataLoaded()
    {
        if (this.model.getApplications().size() == 0)
        {
            ErrorDispatcher.sendError(LauncherErrors.APPLICATION_EMPTY_CONFIG_FILE);
        }
        else if (this.view.getAuthCookie() == null)
        {
            this.view.displayLogin();
            this.view.hideStartPanel();
        }
        else
        {
            this.launchAlreadyLoggedEvent();
        }
    }

    /**
     * Gets authentication cookie values then sets a fake login form to forward via an ALREADY_LOGGED event.
     */
    private void launchAlreadyLoggedEvent()
    {
        ApplicationModelData application = this.selectApplication();
        String user = this.view.getFavCookieValue(LauncherConfig.COOKIE_VAR_USER);

        LoginForm loginForm = new LoginForm(user, "", application, false);
        AppEvent event = new AppEvent(LauncherEvents.ALREADY_LOGGED);
        event.setData(LauncherConfig.EVENT_VAR_LOGIN_FORM, loginForm);

        this.forwardToChild(event);
    }

    /**
     * Check if application to launch is set in the URL. If not, it check if the favorite cookie is set and chose the
     * application written in the cookie. If there is no cookie, it selects the first application of the list.
     * 
     * @return The application that has to be selected by default.
     */
    public ApplicationModelData selectApplication()
    {
        ApplicationModelData application = null;
        String applicationName = this.view.getApplicationParameter();

        if (applicationName == null)
        {
            applicationName = this.view.getFavCookieValue(LauncherConfig.COOKIE_VAR_APPLICATION);
        }

        if (applicationName != null)
        {
            application = this.model.getApplication(applicationName.toLowerCase());
        }

        if (application == null)
        {
            application = this.model.getApplications().get(0);
        }

        return application;
    }

    /**
     * When user is authenticated onLogin sets the authentication cookie in navigator.
     * 
     * @param event
     *            Login event carrying login informations such as encrypted credentials.
     */
    private void onLogin(AppEvent event)
    {
        LoginForm loginForm = event.getData(LauncherConfig.EVENT_VAR_LOGIN_FORM);
        this.view.setFavCookieValue(LauncherConfig.COOKIE_VAR_USER, loginForm.getUserName());
        this.view.setFavCookieValue(LauncherConfig.COOKIE_VAR_USER_ID, loginForm.getUserId().toString());
    }

    /**
     * When logging out, the application view is hidden and the login dialog is displayed.
     */
    private void onLogOut()
    {
        this.view.hideMainPanel();
        this.view.resetFrames();
        this.view.displayLogin();
    }

    /**
     * Changes the application frame URL with the newly selected application URL.
     * 
     * @param event
     */
    private void onLaunch(AppEvent event)
    {
        ApplicationModelData app = event.getData(LauncherConfig.EVENT_VAR_APPLICATION);
        String url = this.buildUrlWithLocaleParameter(app.getUrl());

        this.view.displayApplication(url);
        this.view.setFavCookieValue(LauncherConfig.COOKIE_VAR_APPLICATION, app.getName().toLowerCase());
        this.view.setFavCookieValue(LauncherConfig.COOKIE_VAR_LOCALE, this.model.getLocale());
        this.view.hideStartPanel();
    }

    /**
     * Adds locale parameter to URL
     * 
     * @param appUrl
     *            The URL on which to add the locale parameter.
     * @return appUrl with the locale parameter.
     */
    private String buildUrlWithLocaleParameter(String appUrl)
    {
        appUrl += LanguagesMenuModel.LOCALE_ARG;
        appUrl += this.model.getLocale();

        return appUrl;
    }

    /** Register all events the controller can handle. */
    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(LauncherEvents.LANGUAGES_DATA_LOADED);
        this.registerEventTypes(LauncherEvents.APPLICATIONS_DATA_LOADED);
        this.registerEventTypes(LauncherEvents.IS_LOGGING);
        this.registerEventTypes(LauncherEvents.LAUNCHING);
        this.registerEventTypes(LauncherEvents.LOG_OUT);
        this.registerEventTypes(LauncherEvents.WINDOW_RESIZE);
    }
}
