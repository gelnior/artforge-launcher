package fr.hd3d.launcher.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:
 * 	'/home/frank.rousseau/workspace/Hd3dAppLauncher/src/fr/hd3d/launcher/ui/client/constant/LauncherConstants.properties'.
 */
public interface LauncherConstants extends com.google.gwt.i18n.client.Constants {
  
  /**
   * Translated "All Rights reserved.".
   * 
   * @return translated "All Rights reserved."
   */
  @DefaultStringValue("All Rights reserved.")
  @Key("AllRightReserved")
  String AllRightReserved();

  /**
   * Translated "Application".
   * 
   * @return translated "Application"
   */
  @DefaultStringValue("Application")
  @Key("Application")
  String Application();

  /**
   * Translated "HD3D - Application Launcher".
   * 
   * @return translated "HD3D - Application Launcher"
   */
  @DefaultStringValue("HD3D - Application Launcher")
  @Key("ApplicationLauncher")
  String ApplicationLauncher();

  /**
   * Translated "Changing application will cause all unsaved data lost. ".
   * 
   * @return translated "Changing application will cause all unsaved data lost. "
   */
  @DefaultStringValue("Changing application will cause all unsaved data lost. ")
  @Key("ChangingApplicationWill")
  String ChangingApplicationWill();

  /**
   * Translated "Changing language will cause application reload. ".
   * 
   * @return translated "Changing language will cause application reload. "
   */
  @DefaultStringValue("Changing language will cause application reload. ")
  @Key("ChangingLanguageWill")
  String ChangingLanguageWill();

  /**
   * Translated "LauncherConstants".
   * 
   * @return translated "LauncherConstants"
   */
  @DefaultStringValue("LauncherConstants")
  @Key("ClassName")
  String ClassName();

  /**
   * Translated "Do you still want to change application ? ".
   * 
   * @return translated "Do you still want to change application ? "
   */
  @DefaultStringValue("Do you still want to change application ? ")
  @Key("DoYouStillWantToChangeApplication")
  String DoYouStillWantToChangeApplication();

  /**
   * Translated "Do you still want to change language ? ".
   * 
   * @return translated "Do you still want to change language ? "
   */
  @DefaultStringValue("Do you still want to change language ? ")
  @Key("DoYouStillWantToChangeLanguage")
  String DoYouStillWantToChangeLanguage();

  /**
   * Translated "English".
   * 
   * @return translated "English"
   */
  @DefaultStringValue("English")
  @Key("English")
  String English();

  /**
   * Translated "Error".
   * 
   * @return translated "Error"
   */
  @DefaultStringValue("Error")
  @Key("Error")
  String Error();

  /**
   * Translated "French".
   * 
   * @return translated "French"
   */
  @DefaultStringValue("French")
  @Key("French")
  String French();

  /**
   * Translated "HD3D tools".
   * 
   * @return translated "HD3D tools"
   */
  @DefaultStringValue("HD3D tools")
  @Key("HD3DTools")
  String HD3DTools();

  /**
   * Translated "Help".
   * 
   * @return translated "Help"
   */
  @DefaultStringValue("Help")
  @Key("Help")
  String Help();

  /**
   * Translated "Loading...".
   * 
   * @return translated "Loading..."
   */
  @DefaultStringValue("Loading...")
  @Key("Loading")
  String Loading();

  /**
   * Translated "Log in".
   * 
   * @return translated "Log in"
   */
  @DefaultStringValue("Log in")
  @Key("LogIn")
  String LogIn();

  /**
   * Translated "Open in a new window".
   * 
   * @return translated "Open in a new window"
   */
  @DefaultStringValue("Open in a new window")
  @Key("OpenInNewWindow")
  String OpenInNewWindow();

  /**
   * Translated "Password".
   * 
   * @return translated "Password"
   */
  @DefaultStringValue("Password")
  @Key("Password")
  String Password();

  /**
   * Translated "Please, Wait...".
   * 
   * @return translated "Please, Wait..."
   */
  @DefaultStringValue("Please, Wait...")
  @Key("PleaseWait")
  String PleaseWait();

  /**
   * Translated "Remember Me".
   * 
   * @return translated "Remember Me"
   */
  @DefaultStringValue("Remember Me")
  @Key("RememberMe")
  String RememberMe();

  /**
   * Translated "Select an application...".
   * 
   * @return translated "Select an application..."
   */
  @DefaultStringValue("Select an application...")
  @Key("SelectAnApplication")
  String SelectAnApplication();

  /**
   * Translated "User name".
   * 
   * @return translated "User name"
   */
  @DefaultStringValue("User name")
  @Key("UserName")
  String UserName();

  /**
   * Translated "About".
   * 
   * @return translated "About"
   */
  @DefaultStringValue("About")
  @Key("about")
  String about();

  /**
   * Translated "There was a problem while retrieving applications configuration file. Please reload HD3D Launcher.".
   * 
   * @return translated "There was a problem while retrieving applications configuration file. Please reload HD3D Launcher."
   */
  @DefaultStringValue("There was a problem while retrieving applications configuration file. Please reload HD3D Launcher.")
  @Key("applicationConfigFileError")
  String applicationConfigFileError();

  /**
   * Translated "Are you sure you want to log out ? ".
   * 
   * @return translated "Are you sure you want to log out ? "
   */
  @DefaultStringValue("Are you sure you want to log out ? ")
  @Key("areYouSureYouWantToLogOut")
  String areYouSureYouWantToLogOut();

  /**
   * Translated "Authentication failed. Please verify your login and password.".
   * 
   * @return translated "Authentication failed. Please verify your login and password."
   */
  @DefaultStringValue("Authentication failed. Please verify your login and password.")
  @Key("authenticationFailed")
  String authenticationFailed();

  /**
   * Translated "Company Web site".
   * 
   * @return translated "Company Web site"
   */
  @DefaultStringValue("Company Web site")
  @Key("companyWebsite")
  String companyWebsite();

  /**
   * Translated "There was a problem while retrieving general configuration file. Ensure connection to server is still available.".
   * 
   * @return translated "There was a problem while retrieving general configuration file. Ensure connection to server is still available."
   */
  @DefaultStringValue("There was a problem while retrieving general configuration file. Ensure connection to server is still available.")
  @Key("configurationFileError")
  String configurationFileError();

  /**
   * Translated "confirm".
   * 
   * @return translated "confirm"
   */
  @DefaultStringValue("confirm")
  @Key("confirm")
  String confirm();

  /**
   * Translated "Application launcher can't establish connection to the data server. ".
   * 
   * @return translated "Application launcher can't establish connection to the data server. "
   */
  @DefaultStringValue("Application launcher can't establish connection to the data server. ")
  @Key("dataServerError")
  String dataServerError();

  /**
   * Translated "It seems that no applications are set in the application configuration file. Please correct it then restart.".
   * 
   * @return translated "It seems that no applications are set in the application configuration file. Please correct it then restart."
   */
  @DefaultStringValue("It seems that no applications are set in the application configuration file. Please correct it then restart.")
  @Key("emptyApplicationConfig")
  String emptyApplicationConfig();

  /**
   * Translated "Some fields are empty, please fill them all.".
   * 
   * @return translated "Some fields are empty, please fill them all."
   */
  @DefaultStringValue("Some fields are empty, please fill them all.")
  @Key("emptyFieldsError")
  String emptyFieldsError();

  /**
   * Translated "English".
   * 
   * @return translated "English"
   */
  @DefaultStringValue("English")
  @Key("english")
  String english();

  /**
   * Translated "French".
   * 
   * @return translated "French"
   */
  @DefaultStringValue("French")
  @Key("french")
  String french();

  /**
   * Translated "The server response is invalid. Please verify with your IT Team  that this software version is correct.".
   * 
   * @return translated "The server response is invalid. Please verify with your IT Team  that this software version is correct."
   */
  @DefaultStringValue("The server response is invalid. Please verify with your IT Team  that this software version is correct.")
  @Key("invalidServerReponse")
  String invalidServerReponse();

  /**
   * Translated "There was a problem while retrieving languages configuration file. If you want to chose a different language, please reload HD3D launcher.".
   * 
   * @return translated "There was a problem while retrieving languages configuration file. If you want to chose a different language, please reload HD3D launcher."
   */
  @DefaultStringValue("There was a problem while retrieving languages configuration file. If you want to chose a different language, please reload HD3D launcher.")
  @Key("languageConfigFileError")
  String languageConfigFileError();

  /**
   * Translated "Languages".
   * 
   * @return translated "Languages"
   */
  @DefaultStringValue("Languages")
  @Key("languages")
  String languages();

  /**
   * Translated "Log out".
   * 
   * @return translated "Log out"
   */
  @DefaultStringValue("Log out")
  @Key("logOut")
  String logOut();

  /**
   * Translated "Configuration file seems malformed. Please, correct it.".
   * 
   * @return translated "Configuration file seems malformed. Please, correct it."
   */
  @DefaultStringValue("Configuration file seems malformed. Please, correct it.")
  @Key("malformedConfigurationFileError")
  String malformedConfigurationFileError();

  /**
   * Translated "More...".
   * 
   * @return translated "More..."
   */
  @DefaultStringValue("More...")
  @Key("more")
  String more();

  /**
   * Translated "Options".
   * 
   * @return translated "Options"
   */
  @DefaultStringValue("Options")
  @Key("options")
  String options();
}
