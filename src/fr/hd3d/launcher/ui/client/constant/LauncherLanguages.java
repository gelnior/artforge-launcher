package fr.hd3d.launcher.ui.client.constant;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.client.GWT;


public class LauncherLanguages
{
    /** Constant strings to display : dialog messages, button label... */
    public static LauncherConstants CONSTANTS = GWT.create(LauncherConstants.class);
    /** Parameterizable messages to display (like the counter label) */
    public static LauncherMessages MESSAGES = GWT.create(LauncherMessages.class);

    private final static HashMap<String, String> map = new HashMap<String, String>();

    public static void initLanguages()
    {
        map.put("English", CONSTANTS.English());
        map.put("French", CONSTANTS.French());
    }

    public static String getLanguages(String language)
    {
        return map.get(language);
    }

    public static String getKeyLanguage(String traductedLanguage)
    {
        for (Map.Entry<String, String> entry : map.entrySet())
        {
            String key = entry.getKey();
            String value = entry.getValue();

            if (value.equals(traductedLanguage))
            {
                return key;
            }
        }

        return null;
    }
}
