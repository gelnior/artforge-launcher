package fr.hd3d.launcher.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:
 * 	'/home/frank.rousseau/workspace/Hd3dAppLauncher/src/fr/hd3d/launcher/ui/client/constant/LauncherMessages.properties'.
 */
public interface LauncherMessages extends com.google.gwt.i18n.client.Constants {
  
  /**
   * Translated "LauncherMessages".
   * 
   * @return translated "LauncherMessages"
   */
  @DefaultStringValue("LauncherMessages")
  @Key("ClassName")
  String ClassName();
}
