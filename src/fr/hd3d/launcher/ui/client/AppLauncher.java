package fr.hd3d.launcher.ui.client;

import com.google.gwt.core.client.EntryPoint;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.view.impl.Launcher;


/**
 * Entry point.
 * 
 * Application Launcher is a tool that provides access to all HD3D web applications.
 * 
 * @author Frank Rousseau, HD3D
 */
public class AppLauncher implements EntryPoint
{
    /** View handling the main panel and all other launcher views */
    private final Launcher launcherView = new Launcher();

    /**
     * This is the entry point method. It loads the application in the root document by forwarding the START event. It
     * also registers all application controllers.
     */
    public void onModuleLoad()
    {
        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.addController(launcherView.getController());

        EventDispatcher.forwardEvent(CommonEvents.START);
    }
}
