package fr.hd3d.launcher.ui.client.config;

/**
 * Static variables needed in application.
 * 
 * @author HD3D
 */
public class LauncherConfig
{
    /** Variable name used to carry locale data through event dispatcher. */
    public static final String EVENT_VAR_LOCALE = "locale";
    /** Variable name used to carry application data through event dispatcher. */
    public static final String EVENT_VAR_APPLICATION = "application";
    /** Variable name used by the event handler to carry login form data to the controller */
    public static final String EVENT_VAR_LOGIN_FORM = "login_form";
    /** Applications variable name when carried by event. */
    public static final String APPLICATIONS_EVENT_VAR_NAME = "applications";

    /** CSS class name for application frame styling. */
    public static final String APPLICATION_FRAME_STYLE = "app-frame";

    /** URL parameter name for locale. */
    public static final String URL_VAR_LOCALE = "locale";
    /** Application URL parameter name, used to automatically select the application. */
    public static final String URL_VAR_APPLICATION = "application";

    /** Authentication cookie name */
    public static final String AUTH_COOKIE_NAME = "hd3d_auth_cookie";
    /** Cookie name for last application selected. */
    public static final String FAV_COOKIE_NAME = "hd3d_favorite_cookie";
    /** Variable name for application set in favorite cookie. */
    public static final String COOKIE_VAR_APPLICATION = "application";
    /** Variable name for language set in favorite cookie. */
    public static final String COOKIE_VAR_LOCALE = "locale";
    /** Variable name for user set in favorite cookie. */
    public static final String COOKIE_VAR_USER = "user";    
    /** Variable name for user id set in favorite cookie. */
    public static final String COOKIE_VAR_USER_ID = "userId";

    /** Login variable name needed by login form transmitted to server. */
    public static final String LOGIN_FORM_LOGIN = "login";
    /** Password variable name needed by login form transmitted to server. */
    public static final String LOGIN_FORM_PASSWORD = "password";
    /** Remember me variable name needed by login form transmitted to server. */
    public static final String LOGIN_FORM_REMEMBER = "remember_me";
    /** Remember me variable value needed by login form transmitted to server, if remember option is chosen by user. */
    public static final String LOGIN_FORM_REMEMBER_ON = "on";

    /** Login resource path. */
    public static final String LOGIN_PATH = "../login";
    /** Logout resource path. */
    public static final String LOGOUT_PATH = "../logout";
    /** Name use by the form for the user name text field */
    final public static String USER_FIELD_NAME = "user_name";
    /** Name use by the form for the password text field */
    final public static String PASS_FIELD_NAME = "user_password";
    /** Name use by the form for the password text field */
    final public static String APPLICATION_FIELD_NAME = "application_name";
    /** Separators string between labels and fields */
    final public static String SEPARATOR = " : ";

    /** CSS class for styling login form panel. */
    public static final String LOGIN_FORM_PANEL_STYLE = "login-form-panel";
    /** CSS class for styling login button. */
    public static final String LOGIN_BUTTON_BAR_STYLE = "login-button-bar";
    /** CSS class for styling login status. */
    public static final String LOGIN_STATUS_STYLE = "login-status";

    /** CSS class for styling symbol icon. */
    public static final String LOGO_ICON_STYLE = "logo-icon";
    /** CSS class for styling help icon. */
    public static final String HELP_ICON_STYLE = "help-icon";
    /** CSS class for styling logout icon. */
    public static final String LOGOUT_ICON_STYLE = "logout-icon";

}
