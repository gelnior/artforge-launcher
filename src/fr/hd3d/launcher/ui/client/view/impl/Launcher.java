package fr.hd3d.launcher.ui.client.view.impl;

import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.widget.mainview.MainView;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.constant.LauncherConstants;
import fr.hd3d.launcher.ui.client.constant.LauncherMessages;
import fr.hd3d.launcher.ui.client.controller.LauncherController;
import fr.hd3d.launcher.ui.client.error.LauncherErrors;
import fr.hd3d.launcher.ui.client.listener.LoginWindowResizeListener;
import fr.hd3d.launcher.ui.client.model.LauncherModel;
import fr.hd3d.launcher.ui.client.view.ILauncher;


/**
 * This view display a dialog which contains a login form. It asks for :
 * <ul>
 * <li>User name</li>
 * <li>Password</li>
 * <li>Application to launch first</li>
 * </ul>
 * 
 * @author HD3D
 */
public class Launcher extends MainView implements ILauncher
{
    /** Constant strings to display : dialog messages, button label... */
    public static LauncherConstants CONSTANTS = GWT.create(LauncherConstants.class);
    /** Parameterizable messages to display (like the counter label) */
    public static LauncherMessages MESSAGES = GWT.create(LauncherMessages.class);

    /** Dialog to display (login form). */
    private final LoginDialog loginDialog = new LoginDialog();
    /** Panel containing Menu bar and application frame. */
    private final MainPanel mainPanel = new MainPanel();

    private final LauncherModel model = new LauncherModel();
    private final LauncherController controller = new LauncherController(model, this);

    /** Enable login dialog when a message box is closed. */
    Listener<MessageBoxEvent> infoCallback = new Listener<MessageBoxEvent>() {

        public void handleEvent(MessageBoxEvent be)
        {
            loginDialog.enable();
        }
    };

    /**
     * Default constructor.
     */
    public Launcher()
    {
        super(CONSTANTS.ApplicationLauncher());

        this.controller.addChild(loginDialog.getController());
        this.controller.addChild(mainPanel.getMenuBar().getController());

        this.addFrameToRootPanel();
        this.hideMainPanel();

        this.setListeners();
    }

    /**
     * @return Launcher main view controller.
     */
    public LauncherController getController()
    {
        return this.controller;
    }

    @Override
    public void init()
    {}

    /** Display login dialog. */
    public void displayLogin()
    {
        this.loginDialog.show();
        this.loginDialog.center();
    }

    /** Display application. */
    public void displayApplication(String appUrl)
    {
        if (this.loginDialog != null)
        {
            this.loginDialog.hide();
            this.mainPanel.show();
        }

        this.mainPanel.setFrameUrl(appUrl);
    }

    /**
     * Hides frame where application is settled.
     */
    public void hideMainPanel()
    {
        this.mainPanel.hide();
    }

    /**
     * If login dialog is visible, it sets the dialog to the center of the navigator window.
     */
    public void centerLoginDialog()
    {
        if (loginDialog != null && loginDialog.isVisible())
        {
            loginDialog.center();
        }
    }

    /** @return locale parameter from navigator URL. */
    public String getLocaleParameter()
    {
        return Window.Location.getParameter(LauncherConfig.URL_VAR_LOCALE);
    }

    /** Return host on which the launcher is running. */
    public String getHost()
    {
        return Window.Location.getHost();
    }

    /**
     * Get authorization cookie value.
     * 
     * @see fr.hd3d.launcher.ui.client.view.ILauncher#getAuthCookie()
     */
    public String getAuthCookie()
    {
        return Cookies.getCookie(LauncherConfig.AUTH_COOKIE_NAME);
    }

    /**
     * Get application parameter from URL.
     * 
     * @see fr.hd3d.launcher.ui.client.view.ILoginDialog#getApplicationParameter()
     */
    public String getApplicationParameter()
    {
        return Window.Location.getParameter(LauncherConfig.URL_VAR_APPLICATION);
    }

    /**
     * Display a specific message if an error occurs.
     * 
     * @param error
     *            Error type needed to display the right message.
     */
    @Override
    public void displayError(Integer error, String userMsg, String stack)
    {
        super.displayError(error, userMsg, stack);

        switch (error)
        {
            case LauncherErrors.APPLICATIONS_LOAD:
                this.displayInfo(CONSTANTS.Error(), CONSTANTS.applicationConfigFileError());
                break;
            case LauncherErrors.LANGUAGES_LOAD:
                this.displayInfo(CONSTANTS.Error(), CONSTANTS.languageConfigFileError());
                break;

            case LauncherErrors.INVALID_LOGIN_FORM:
                this.displayInfo(CONSTANTS.Error(), CONSTANTS.emptyFieldsError());
                break;
            case LauncherErrors.INVALID_CREDENTIALS:
                this.displayInfo(CONSTANTS.Error(), CONSTANTS.authenticationFailed());
                break;
            case LauncherErrors.APPLICATION_EMPTY_CONFIG_FILE:
                this.displayInfo(CONSTANTS.Error(), CONSTANTS.emptyApplicationConfig());
                break;
            case LauncherErrors.INVALID_SERVER_RESPONSE:
                this.displayInfo(CONSTANTS.Error(), CONSTANTS.invalidServerReponse());
                break;
            default:
                ;
        }
    }

    /**
     * Display an informative message box.
     * 
     * @param title
     *            The message box title.
     * @param msg
     *            The message to display.
     */
    public void displayInfo(String title, String msg)
    {
        if (this.loginDialog.isVisible())
        {
            this.loginDialog.disable();
        }
        MessageBox.info(title, msg, infoCallback);
    }

    /**
     * Clear navigator window.
     */
    public void clear()
    {
        RootPanel.get().clear();
    }

    /**
     * @param key
     *            The key of the value to return.
     * @return the value stored in the favorite cookie corresponding to <i>key</i>.
     */
    public String getFavCookieValue(String key)
    {
        return FavoriteCookie.getFavParameterValue(key);
    }

    /**
     * Create or update the couple key-value in the favorite cookie.
     * 
     * @param value
     *            the value to set in favorite cookie.
     * @param key
     *            The key of the value to return.
     */
    public void setFavCookieValue(String key, String value)
    {
        FavoriteCookie.putFavValue(key, value);
    }

    /** Delete the favorite cookie. */
    public void removeFavCookie()
    {
        Cookies.removeCookie(LauncherConfig.FAV_COOKIE_NAME);
    }

    /**
     * Sets the whole stuff (menu bar and application) into the navigator window.
     */
    private void addFrameToRootPanel()
    {
        this.hideStartPanel();
        this.addToViewport(mainPanel);
    }

    /**
     * Sets listeners on view elements.
     */
    private void setListeners()
    {
        Window.addResizeHandler(new LoginWindowResizeListener());
    }

    public void resetFrames()
    {
        this.mainPanel.clearFrames();
    }
}
