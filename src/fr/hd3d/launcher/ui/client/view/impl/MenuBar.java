package fr.hd3d.launcher.ui.client.view.impl;

import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.constant.LauncherConstants;
import fr.hd3d.launcher.ui.client.constant.LauncherMessages;
import fr.hd3d.launcher.ui.client.controller.MenuBarController;
import fr.hd3d.launcher.ui.client.listener.AboutListener;
import fr.hd3d.launcher.ui.client.listener.GoOnlineHelpListener;
import fr.hd3d.launcher.ui.client.listener.LogOutListener;
import fr.hd3d.launcher.ui.client.listener.YesChangeApplicationListener;
import fr.hd3d.launcher.ui.client.listener.YesChangeLanguageListener;
import fr.hd3d.launcher.ui.client.listener.YesLogOutListener;
import fr.hd3d.launcher.ui.client.model.MenuBarModel;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.client.view.IMenuBar;


/**
 * View used to display menu bar and its elements.
 * 
 * @author HD3D
 */
public class MenuBar implements IMenuBar
{
    /** Constant strings to display : dialog messages, button label... */
    public static LauncherConstants CONSTANTS = GWT.create(LauncherConstants.class);
    /** Parameterizable messages to display (like the counter label) */
    public static LauncherMessages MESSAGES = GWT.create(LauncherMessages.class);

    /** Menu bar data */
    private final MenuBarModel model = new MenuBarModel();
    /** Widget controller. */
    private final MenuBarController controller = new MenuBarController(model, this);

    /** Tool bar to display */
    private final ToolBar toolBar = new ToolBar();

    /** Symbol displays on the left of the menu bar */
    private final Button symbolButton = new Button();
    /** Options menu (handles languages change by default) */
    private final LanguagesMenu languagesMenu = new LanguagesMenu();
    /** Application menus */
    private AppMenu appMenu;
    /** User name label */
    private LabelToolItem userNameItem;

    /** Default Constructor */
    public MenuBar()
    {

        this.setToolbar();
    }

    /** The widget controller */
    public Controller getController()
    {
        return controller;
    }

    /**
     * Display running application about text with the HD3D symbol.
     * 
     * @param aboutText
     */
    public void displayAbout(String aboutText)
    {
        final Dialog aboutDialog = new Dialog();
        aboutDialog.setHeading(CONSTANTS.about());
        aboutDialog.setButtons(Dialog.OK);

        aboutDialog.addText(aboutText + "<br /><br />");
        aboutDialog.add(Hd3dImages.hd3dSmallSymbol());
        aboutDialog.addText(CONSTANTS.AllRightReserved());
        aboutDialog.setScrollMode(Scroll.AUTO);
        aboutDialog.setHideOnButtonClick(true);
        aboutDialog.setBodyStyle("padding: 10px;");
        aboutDialog.getButtonBar().setStyleAttribute("margin-left", "13px;");

        aboutDialog.show();
    }

    /**
     * Display a dialog box to ask for confirmation before changing language.
     * 
     * @param selectedLocale
     *            the selected locale
     */
    public void displayConfirmLanguageChange(String selectedLocale)
    {
        String msg = CONSTANTS.ChangingLanguageWill();
        msg += CONSTANTS.DoYouStillWantToChangeLanguage();

        this.displayConfirmation(msg, new YesChangeLanguageListener(selectedLocale));
    }

    /**
     * Display a message box to ask for confirmation before changing application.
     * 
     * @param app
     *            the selected application
     */
    public void displayConfirmAppChange(ApplicationModelData app)
    {
        String msg = CONSTANTS.ChangingApplicationWill();
        msg += CONSTANTS.DoYouStillWantToChangeApplication();

        this.displayConfirmation(msg, new YesChangeApplicationListener(app));
    }

    /** Displays a message box asking for confirmation before logging out. */
    public void displayConfirmLogOut()
    {
        String msg = CONSTANTS.areYouSureYouWantToLogOut();
        this.displayConfirmation(msg, new YesLogOutListener());
    }

    /**
     * Change language by changing URL parameter ?locale=...
     * */
    public void changeLanguage(String urlParameters)
    {
        String url = Window.Location.getPath() + "?";
        url += urlParameters;
        Window.Location.replace(url);
    }

    /**
     * Open a new navigator window which go to the URL passed in parameter.
     * 
     * @param url
     *            URL to go to.
     */
    public void openWebsite(String url)
    {
        Window.open(url, MenuBarModel.HELP_WINDOW_NAME, MenuBarModel.NEW_WINDOW_SETTINGS);
    }

    /** Get widget handled by this view. */
    public ToolBar getWidget()
    {
        return this.toolBar;
    }

    /**
     * Insert tool bar item at specified index.
     * 
     * @param item
     * @param index
     */
    public void insertItem(Component item, int index)
    {
        this.toolBar.insert(item, index);
    }

    /** Refresh the application menu. */
    public void refreshAppMenu()
    {
        this.appMenu.refresh();
    }

    /** Refresh the user name label. */
    public void refreshUserName()
    {
        String style = "font-weight: bold;  margin-right: 2px; ";
        String htmlSpan = "<span style=\"" + style + "\">" + this.model.getUserName() + "</span>";

        this.userNameItem.setLabel(htmlSpan);
    }

    /** Get from navigator as a map parameters set in the URL. */
    public Map<String, List<String>> getUrlParameterMap()
    {
        return Window.Location.getParameterMap();
    }

    /**
     * Open a new browser window with the url set on the launcher.
     * 
     * @param parameters
     *            Parameters to add to url
     */
    public void openLauncher(String parameters, String windowName)
    {
        Window.open(Window.Location.getPath() + "?" + parameters, windowName, MenuBarModel.NEW_WINDOW_SETTINGS);
    }

    /**
     * Displays a message box asking for confirmation.
     * 
     * @param msg
     *            the message to display.
     * @param listener
     *            the listener to set on.
     */
    private void displayConfirmation(String msg, Listener<MessageBoxEvent> listener)
    {
        String confirm = CONSTANTS.confirm();
        MessageBox.confirm(confirm, msg, listener);
    }

    /** Set all tool bar elements. */
    private void setToolbar()
    {
        this.setSymbol();
        this.setApplicationsMenu();
        this.toolBar.add(new FillToolItem());

        this.setUserName();
//        this.toolBar.add(new SeparatorToolItem());
        this.setOptionsMenu();
//        this.setHelpButton();
        this.toolBar.add(new SeparatorToolItem());
        this.setDisconectButton();

        this.toolBar.setStyleAttribute("padding", "5px");
    }

    /** Build up applications menu. */
    private void setApplicationsMenu()
    {
        appMenu = new AppMenu(this, this.model);
    }

    /** Build up options menu. */
    private void setOptionsMenu()
    {
        this.controller.addChild(languagesMenu.getController());

        // this.toolBar.add(this.languagesMenu);
    }

    /** Build up symbol. */
    private void setSymbol()
    {
        symbolButton.setIconStyle(LauncherConfig.LOGO_ICON_STYLE);
        symbolButton.setToolTip(CONSTANTS.about());
        symbolButton.addSelectionListener(new AboutListener());

        this.toolBar.add(symbolButton);
    }

    /** Build up user name item. */
    private void setUserName()
    {
        String style = "font-weight: bold;  margin-right: 5px; ";
        String htmlSpan = "<span style=\"" + style + "\">" + this.model.getUserName() + "</span>";

        this.userNameItem = new LabelToolItem(htmlSpan);
        this.toolBar.add(userNameItem);
    }

    /** Build up help button. */
    private void setHelpButton()
    {
        Button helpItem = new Button();
        helpItem.setIconStyle(LauncherConfig.HELP_ICON_STYLE);
        helpItem.setToolTip(CONSTANTS.Help());
        helpItem.addSelectionListener(new GoOnlineHelpListener());

        this.toolBar.add(helpItem);
    }

    /** Build up log out button. */
    private void setDisconectButton()
    {
        Button logoutButton = new Button();
        logoutButton.setIconStyle(LauncherConfig.LOGOUT_ICON_STYLE);
        logoutButton.setToolTip(CONSTANTS.logOut());
        logoutButton.addSelectionListener(new LogOutListener());
        logoutButton.setId("logout-button");

        this.toolBar.add(logoutButton);
    }

}
