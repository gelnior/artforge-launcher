package fr.hd3d.launcher.ui.client.view.impl;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.menu.SeparatorMenuItem;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.launcher.ui.client.constant.LauncherConstants;
import fr.hd3d.launcher.ui.client.constant.LauncherMessages;
import fr.hd3d.launcher.ui.client.listener.ChangeApplicationListener;
import fr.hd3d.launcher.ui.client.listener.ChangeApplicationToolbarListener;
import fr.hd3d.launcher.ui.client.listener.NewWindowApplicationListener;
import fr.hd3d.launcher.ui.client.model.MenuBarModel;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * Application menu is split in three parts :
 * <ul>
 * <li>Main application name</li>
 * <li>Application items from the same family</li>
 * <li>Foreign application menu</li>
 * </ul>
 * 
 * @author HD3D
 */
public class AppMenu
{
    /** Constant strings to display : dialog messages, button label... */
    public static LauncherConstants CONSTANTS = GWT.create(LauncherConstants.class);
    /** Parameterizable messages */
    public static LauncherMessages MESSAGES = GWT.create(LauncherMessages.class);

    /** Menu Bar in which application menu should be included */
    private final MenuBar menuBar;
    /** Menu bar data */
    private final MenuBarModel model;

    /** Item used for the foreign application menu. */
    private final Button foreignAppMenuItem = new Button(CONSTANTS.more());
    /** Menu displaying all application which are not from the main family application. */
    private final Menu foreignAppMenu = new Menu();
    /** Item used to display the main application name. */
    private LabelToolItem mainAppItem;
    /** List of application items from the same family. */
    private final ArrayList<Button> familyItems = new ArrayList<Button>();
    /** List of application items not from the same family. */
    private ArrayList<ApplicationModelData> foreignApps;

    // private final FastMap<MenuItem> moreMenuItem = new FastMap<MenuItem>();
    // private MenuItem lastSelectedMenuItem;

    /**
     * Default constructor.
     * 
     * @param menuBar
     *            Menu Bar in which application menu should be included
     * @param model
     *            Menu bar data
     */
    public AppMenu(MenuBar menuBar, MenuBarModel model)
    {
        this.menuBar = menuBar;
        this.model = model;
    }

    /** Refresh Menu. */
    public void refresh()
    {
        this.removeItems();
        this.addItemsToMenuBar();
    }

    /** Remove all items from the menu bar. */
    private void removeItems()
    {
        this.removeMainAppItem();
        this.removeMainAppFamilyItems();
        this.removeForeignAppMenu();
    }

    /** Empty foreign application menu. */
    private void removeForeignAppMenu()
    {
        this.foreignAppMenu.hide();
        // this.foreignAppMenu.removeAll();
    }

    /** Remove items from the main application family. */
    private void removeMainAppFamilyItems()
    {
        for (Button item : this.familyItems)
        {
            this.menuBar.getWidget().remove(item);
        }
        this.familyItems.clear();
    }

    /** Remove item used to display the running application name. */
    private void removeMainAppItem()
    {
        if (this.mainAppItem != null)
        {
            this.menuBar.getWidget().remove(this.mainAppItem);
        }
    }

    /**
     * Set all items from applications menu in the main menu bar : direct tool items for the main application family and
     * menu for the foreign application.
     */
    private void addItemsToMenuBar()
    {
        this.setForeignAppsMenu();
        this.setMainAppFamilyItems();
    }

    /** Set items for each application from the main application family. */
    private void setMainAppFamilyItems()
    {
        ArrayList<ApplicationModelData> familyApps = this.model.getFamilyApps();

        for (ApplicationModelData app : familyApps)
        {
            if (app != this.model.getMainApp())
            {
                Button appItem = new Button(app.getName());
                appItem.addSelectionListener(new ChangeApplicationToolbarListener(app));
                this.setAppContextMenu(app, appItem);

                this.menuBar.insertItem(appItem, 1);
                this.familyItems.add(appItem);
            }
            else
            {
                String style = "font-size: 16px; font-weight: bold; margin-left: 5px; margin-right: 5px; ";
                String htmlSpan = "<span style=\"" + style + "\">" + app.getName() + "</span>";

                mainAppItem = new LabelToolItem(htmlSpan);
                mainAppItem.setStyleName(MenuBarModel.MAIN_APP_STYLE);

                this.menuBar.insertItem(mainAppItem, 1);
            }
        }
    }

    /**
     * Set a context menu with the open in new window item on the <i>appItem</i>
     * 
     * @param app
     *            The application to set in the new window.
     * @param appItem
     *            The item on which the context menu will be set.
     */
    private void setAppContextMenu(ApplicationModelData app, Button appItem)
    {
        Menu menu = new Menu();
        MenuItem newWindowMenuItem = new MenuItem(CONSTANTS.OpenInNewWindow());
        newWindowMenuItem.addSelectionListener(new NewWindowApplicationListener(app));
        menu.add(newWindowMenuItem);

        appItem.setContextMenu(menu);
    }

    /**
     * Set menu for applications which are not from the main application family.
     */
    private void setForeignAppsMenu()
    {
        this.foreignAppMenu.removeAll();
        FastMap<List<ApplicationModelData>> familyMap = new FastMap<List<ApplicationModelData>>();

        for (ApplicationModelData app : model.getForeignApps())
        {
            if (app.getFamily() != null)
            {
                if (familyMap.get(app.getFamily()) == null)
                {
                    List<ApplicationModelData> familyList = new ArrayList<ApplicationModelData>();
                    familyMap.put(app.getFamily(), familyList);
                }

                familyMap.get(app.getFamily()).add(app);
            }
        }

        if (foreignAppMenuItem.getMenu() == null)
        {
            foreignAppMenuItem.setMenu(foreignAppMenu);
        }

        this.foreignApps = this.model.getForeignApps();

        if (this.foreignApps.size() > 0)
        {
            for (String family : familyMap.keySet())
            {
                if (foreignAppMenu.getItemCount() > 0)
                {
                    foreignAppMenu.add(new SeparatorMenuItem());
                }

                List<ApplicationModelData> familyList = familyMap.get(family);
                for (ApplicationModelData app : familyList)
                {
                    MenuItem appItem = new MenuItem(app.getName());
                    appItem.addSelectionListener(new ChangeApplicationListener(app));
                    foreignAppMenu.add(appItem);

                    // moreMenuItem.put(app.getName(), appItem);
                }

                this.menuBar.insertItem(foreignAppMenuItem, 1);
            }
        }
    }
}
