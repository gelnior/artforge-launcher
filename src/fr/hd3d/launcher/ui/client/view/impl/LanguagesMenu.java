package fr.hd3d.launcher.ui.client.view.impl;

import java.util.ArrayList;

import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.menu.CheckMenuItem;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;

import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.constant.LauncherConstants;
import fr.hd3d.launcher.ui.client.constant.LauncherLanguages;
import fr.hd3d.launcher.ui.client.controller.LanguagesMenuController;
import fr.hd3d.launcher.ui.client.listener.ChangeLanguageListener;
import fr.hd3d.launcher.ui.client.model.LanguagesMenuModel;
import fr.hd3d.launcher.ui.client.model.modeldata.LanguageModelData;
import fr.hd3d.launcher.ui.client.view.ILanguagesMenu;


/**
 * Options menu. By default, handles language changing.
 * 
 * @author HD3D
 * 
 */
public class LanguagesMenu extends Button implements ILanguagesMenu
{
    /** Constant strings to display : dialog messages, button label... */
    public static LauncherConstants CONSTANTS = GWT.create(LauncherConstants.class);

    /** Model that handles menu bar data */
    private final LanguagesMenuModel model = new LanguagesMenuModel();
    /** Widget controller. */
    private final LanguagesMenuController controller = new LanguagesMenuController(this, model);

    /** Options menu */
    private final Menu languagesMenu = new Menu();

    /**
     * Default Constructor.
     * 
     * @param model
     *            Data model
     */
    public LanguagesMenu()
    {
        super(CONSTANTS.languages());

        LauncherLanguages.initLanguages();
        this.setLanguageMenu();
    }

    public Controller getController()
    {
        return controller;
    }

    /**
     * Check the language given in parameter and uncheck all other language. This action does not raise event.
     * 
     * @param language
     *            the language to check in the language menu.
     */
    public void checkLanguage(String language)
    {
        for (Component item : this.languagesMenu.getItems())
        {
            if (((CheckMenuItem) item).getText().compareTo(language) == 0)
            {
                ((CheckMenuItem) item).setChecked(true);
            }
            else
            {
                ((CheckMenuItem) item).setChecked(false);
            }
        }
    }

    /**
     * Fill language menu with language stored by the model.
     */
    public void fill()
    {
        if (this.model.getLanguages().size() > 0)
        {
            this.setLanguagesItems(this.model.getLanguages());
        }
        else
        {
            this.hide();
        }
    }

    /** @return locale parameter from navigator URL. */
    public String getLocaleParameter()
    {
        return Window.Location.getParameter(LauncherConfig.URL_VAR_LOCALE);
    }

    /**
     * Build the language selection menu.
     */
    private void setLanguageMenu()
    {
        this.setMenu(languagesMenu);
    }

    /**
     * Add item for each language supported by the application.
     * 
     * @param langMap
     *            the map giving all languages supported.
     */
    private void setLanguagesItems(ArrayList<LanguageModelData> languages)
    {
        ChangeLanguageListener<MenuEvent> listener = new ChangeLanguageListener<MenuEvent>(this.model);

        for (LanguageModelData language : languages)
        {
            boolean checked = false;
            boolean enabled = true;

            if (language.getLocale().compareTo(this.model.getLocale()) == 0)
            {
                checked = true;
                enabled = false;
            }
            this.addLangItem(LauncherLanguages.getLanguages(language.getName()), checked, enabled, listener);
        }
    }

    /**
     * Add an item entry to the language menu.
     * 
     * @param language
     *            Language to add.
     * @param enabled
     *            True if item will be enabled.
     * @param checked
     *            True if item will be checked.
     * @param listener
     *            listener to set on the item.
     */
    private void addLangItem(String language, boolean checked, boolean enabled,
            ChangeLanguageListener<MenuEvent> listener)
    {
        CheckMenuItem radioMenuItem = new CheckMenuItem(language);
        radioMenuItem.setChecked(checked);
        radioMenuItem.setGroup(LanguagesMenuModel.GROUP_LANGUAGES);
        radioMenuItem.addSelectionListener(listener);
        radioMenuItem.setEnabled(enabled);

        this.languagesMenu.add(radioMenuItem);
    }
}
