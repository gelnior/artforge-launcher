package fr.hd3d.launcher.ui.client.view.impl;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.layout.CardLayout;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.user.client.ui.Frame;

import fr.hd3d.launcher.ui.client.config.LauncherConfig;


/**
 * <p>
 * This widget implements the main panel that every HD3D GXT applications should use. The main panel is constituted of a
 * menu bar on the top and dock panel on bottom. The menu bar includes :
 * </p>
 * <ul>
 * <li>A symbol (default is an HD3D symbol)</li>
 * <li>An application menu to change application</li>
 * <li>An user name label</li>
 * <li>A language menu</li>
 * <li>An help menu</li>
 * <li>A disconnection button</li>
 * </ul>
 * 
 * @author HD3D
 */
public class MainPanel extends ContentPanel
{
    /** MenuBar widget */
    private final MenuBar menuBar = new MenuBar();

    /** Application frame */
    private final FastMap<LayoutContainer> frameMap = new FastMap<LayoutContainer>();
    /** Panel layout. */
    final CardLayout layout = new CardLayout();

    /**
     * Default constructor.
     */
    public MainPanel()
    {
        this.setMenuBar();
        this.setStyle();
    }

    /**
     * @return The menu bar of the main panel.
     */
    public MenuBar getMenuBar()
    {
        return menuBar;
    }

    /**
     * Change the frame. If the frame associated with <i>url</i> already exists, it shows it. Else it creates a new one,
     * add it to the frame map then shows it.
     * 
     * @param url
     *            the new URL.
     */
    public void setFrameUrl(String url)
    {
        if (frameMap.get(url) == null)
        {
            this.setFrame(url);
        }
        this.layout.setActiveItem(frameMap.get(url));

        if (!this.isVisible())
        {
            this.show();
        }
    }

    /**
     * Set styles such as no borders and border layout.
     * */
    private void setStyle()
    {
        this.setLayout(layout);
        this.setHeaderVisible(false);
        this.setBorders(false);
        this.setBodyBorder(false);
        this.setFrame(false);
    }

    /** Set menu bar to the north of the panel. */
    private void setMenuBar()
    {
        this.setTopComponent(this.menuBar.getWidget());
    }

    /**
     * Build a frame that displays the URL given in parameter. Add it to the frame map.
     * 
     * @param url
     *            The frame URL.
     */
    private void setFrame(String url)
    {
        LayoutContainer container = new LayoutContainer();
        container.setLayout(new FitLayout());

        Frame frame = new Frame();
        frame.setStyleName(LauncherConfig.APPLICATION_FRAME_STYLE);
        frame.setUrl(url);

        container.add(frame);
        this.add(container);
        frameMap.put(url, container);
    }

    public void clearFrames()
    {
        this.frameMap.clear();
    }
}
