package fr.hd3d.launcher.ui.client.view.impl;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.binding.FieldBinding;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Status;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.constant.LauncherConstants;
import fr.hd3d.launcher.ui.client.constant.LauncherMessages;
import fr.hd3d.launcher.ui.client.controller.LoginController;
import fr.hd3d.launcher.ui.client.listener.LoginClickListener;
import fr.hd3d.launcher.ui.client.listener.LoginDialogKeyListener;
import fr.hd3d.launcher.ui.client.model.LoginForm;
import fr.hd3d.launcher.ui.client.model.LoginModel;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.client.view.ILoginDialog;


/**
 * Simple login form based on GXT widgets including : *
 * <ul>
 * <li>One field for the user name</li>
 * <li>One field for the password</li>
 * <li>One button to submit</li>
 * </ul>
 * 
 * @author Frank Rousseau
 */
public class LoginDialog extends Dialog implements ILoginDialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static LauncherConstants CONSTANTS = GWT.create(LauncherConstants.class);
    /** Parameterizable messages */
    public static LauncherMessages MESSAGES = GWT.create(LauncherMessages.class);
    /** Model to handle login form data. */
    private final LoginModel model = new LoginModel();
    /** Login dialog controller. */
    private final LoginController controller = new LoginController(model, this);;

    /** User name text field. Invalid if empty. */
    private final TextField<String> userNameTextField = new TextField<String>();
    /** Password text field. Invalid if empty. */
    private final TextField<String> passwordTextField = new TextField<String>();
    /** Application combo box : to select the application to launch. */
    private final ComboBox<ApplicationModelData> appCombo = new ComboBox<ApplicationModelData>();
    /** Remember me check box to set long live on the authentication cookie. */
    private final CheckBox rememberMeCheckBox = new CheckBox();

    /** Form panel */
    private final LayoutContainer formPanel = new LayoutContainer() {};

    /** Status label to show loading informations */
    protected Status status;
    /** The button that raise the login clicked event. */
    private final Button loginButton = new Button(CONSTANTS.LogIn());

    /** When enter key is pressed up, it has the same behavior that when Button is clicked. */
    private final LoginDialogKeyListener enterKeyListener = new LoginDialogKeyListener(loginButton);

    /** Initialize the form. */
    public LoginDialog()
    {
        this.setDialogStyle();
        this.initForm();
        super.createButtons();
        this.setStatus();
        this.setLoginButton();
        this.setBinders();
        this.setElementIds();

        this.addListener(Events.Enable, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                userNameTextField.setSelectOnFocus(true);
                userNameTextField.focus();
                enterKeyListener.enable();
            }
        });
    }

    /** Set binders between model form and form fields. */
    private void setBinders()
    {
        FieldBinding userBinder = new FieldBinding(userNameTextField, LoginForm.USER_NAME_FIELD);
        userBinder.bind(this.model.getLoginForm());
        FieldBinding passwordBinder = new FieldBinding(passwordTextField, LoginForm.PASSWORD_FIELD);
        passwordBinder.bind(this.model.getLoginForm());
        ModelComboBoxFieldBinding<ApplicationModelData> applicationBinder = new ModelComboBoxFieldBinding<ApplicationModelData>(
                appCombo, LoginForm.APPLICATION_FIELD);
        applicationBinder.bind(this.model.getLoginForm());
        FieldBinding rememberMeBinder = new FieldBinding(rememberMeCheckBox, LoginForm.REMEMBER_ME_FIELD);
        rememberMeBinder.bind(this.model.getLoginForm());
    }

    /** Get dialog controller. */
    public LoginController getController()
    {
        return this.controller;
    }

    /**
     * @return User name from the text field.
     */
    public String getUserName()
    {
        return this.userNameTextField.getValue();
    }

    /**
     * @return password from the the text field.
     */
    public String getPassword()
    {
        return this.passwordTextField.getValue();
    }

    /**
     * @return Application selected in the combo box.
     */
    public ApplicationModelData getApplication()
    {
        if (this.appCombo.getSelection().size() > 0)
        {
            return this.appCombo.getSelection().get(0);
        }
        else
        {
            return null;
        }
    }

    /**
     * @return True if remember value is checked.
     */
    public boolean getRememberMe()
    {
        return rememberMeCheckBox.getValue();
    }

    /**
     * @return true if all fields are non-empty.
     */
    public boolean isValid()
    {
        boolean testUserName = this.userNameTextField.getValue() != null;
        boolean testPassword = this.passwordTextField.getValue() != null;
        boolean testApplication = this.appCombo.getValue() != null;

        return testUserName && testPassword && testApplication;
    }

    /**
     * Reset fields values.
     */
    public void reset()
    {
        this.userNameTextField.reset();
        this.passwordTextField.reset();
        this.appCombo.reset();
        this.rememberMeCheckBox.reset();
    }

    /**
     * Select in the combo box the application given in parameter.
     * 
     * @param application
     *            the application to select.
     */
    public void selectApplication(ApplicationModelData application)
    {
        if (application != null)
        {
            this.appCombo.setValue(application);
        }
    }

    /**
     * @return Application parameter value set in the URL.
     * 
     * @see fr.hd3d.launcher.ui.client.view.ILoginDialog#getApplicationParameter()
     */
    public String getApplicationParameter()
    {
        return Window.Location.getParameter(LauncherConfig.URL_VAR_APPLICATION);
    }

    /**
     * @return HD3D favorite (chocolate) cookie that contains the name of the last launched application .
     * 
     *         (non-Javadoc)
     * @see fr.hd3d.launcher.ui.client.view.ILoginDialog#getFavCookie()
     */
    public String getFavCookie()
    {
        return Cookies.getCookie(LauncherConfig.FAV_COOKIE_NAME);
    }

    /**
     * Hide loading message in status bar and enable login button.
     */
    public void hideLoading()
    {
        this.getButtonBar().enable();
        this.status.hide();
    }

    /**
     * Show loading message in status bar and disable login button.
     */
    public void showLoading()
    {
        this.status.show();
        this.getButtonBar().disable();
    }

    /**
     * Set comboxBox Application Not visible.
     */
    public void hideApplicationCombobox()
    {
        this.appCombo.setVisible(false);
    }

    /** Set combo box which allow user to select the application he wants. */
    private void setAppComboBox()
    {
        this.appCombo.setFieldLabel(CONSTANTS.Application());
        this.appCombo.setName(LauncherConfig.APPLICATION_FIELD_NAME);
        this.appCombo.setTriggerAction(TriggerAction.ALL);
        this.appCombo.setTypeAhead(true);
        this.appCombo.setDisplayField(ApplicationModelData.NAME);
        this.appCombo.setEmptyText(CONSTANTS.SelectAnApplication());
        this.appCombo.setWidth(150);
        this.appCombo.addKeyListener(enterKeyListener);

        ListStore<ApplicationModelData> appStore = this.model.getComboStore();
        this.appCombo.setStore(appStore);
        this.formPanel.add(appCombo);
    }

    /** Set up field set and button. */
    protected void initForm()
    {
        this.setFormPanelStyle();

        HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.add(Hd3dImages.hd3dSymbol());
        hPanel.add(this.formPanel);
        this.add(hPanel);
    }

    /** Set dialog box style : title, width, not closable, modal, not resizable. */
    protected void setDialogStyle()
    {
        this.setButtonAlign(HorizontalAlignment.LEFT);
        this.setButtons("");
        this.setWidth(500);

        this.setClosable(false);
        this.setModal(true);
        this.setResizable(false);

        String style = "padding: 20px;";
        this.getHeader().setStyleName("login-header");
        this.setBodyStyle(style);
    }

    /** Set form panel style. */
    protected void setFormPanelStyle()
    {
        FormLayout layout = new FormLayout();

        layout.setLabelWidth(100);
        layout.setDefaultWidth(150);
        layout.setLabelSeparator(LauncherConfig.SEPARATOR);
        this.formPanel.setLayout(layout);

        this.formPanel.setStyleName(LauncherConfig.LOGIN_FORM_PANEL_STYLE);
        this.setLoginFormFieldSet();
    }

    /** Set the field set with three fields, user name, password and the application. */
    protected void setLoginFormFieldSet()
    {
        this.setUserNameTextField();
        this.setPasswordTextField();
        this.setAppComboBox();
        // Temporarly remove because services does not handle it.
        // this.setRememberMeCheckBox();
    }

    /** Set the text field user name with no blank allowed. */
    protected void setUserNameTextField()
    {
        this.userNameTextField.setFieldLabel(CONSTANTS.UserName());
        this.userNameTextField.setMinLength(2);
        this.userNameTextField.setName(LauncherConfig.USER_FIELD_NAME);
        this.userNameTextField.addKeyListener(enterKeyListener);

        this.formPanel.add(userNameTextField);
        this.setFocusWidget(userNameTextField);
    }

    /** Set the text field pass word with no blank allowed. */
    protected void setPasswordTextField()
    {
        this.passwordTextField.setFieldLabel(CONSTANTS.Password());
        this.passwordTextField.setMinLength(2);
        this.passwordTextField.setName(LauncherConfig.PASS_FIELD_NAME);
        this.passwordTextField.setPassword(true);
        this.passwordTextField.addKeyListener(enterKeyListener);

        this.formPanel.add(passwordTextField);
    }

    /** Set the remember check box. */
    protected void setRememberMeCheckBox()
    {
        this.rememberMeCheckBox.setFieldLabel(CONSTANTS.RememberMe());
        this.rememberMeCheckBox.setStyleAttribute("float", "right");

        this.formPanel.add(this.rememberMeCheckBox);
    }

    /**
     * Sets status label to show when dialog box is loading.
     */
    private void setStatus()
    {
        this.status = new Status();
        this.status.setBusy(CONSTANTS.PleaseWait());
        this.status.setStyleName(LauncherConfig.LOGIN_STATUS_STYLE);
        this.status.hide();
        this.status.setAutoWidth(true);
        this.getButtonBar().add(status);
        this.getButtonBar().add(new FillToolItem());
    }

    /**
     * Set the login button at the bottom of the form. When clicked, it launches the selected application.
     */
    protected void setLoginButton()
    {
        this.loginButton.addSelectionListener(new LoginClickListener());
        this.getButtonBar().setStyleName(LauncherConfig.LOGIN_BUTTON_BAR_STYLE);

        this.getButtonBar().add(loginButton);
    }

    /** Set HTML IDs on login form to facilitate acceptation tests. */
    private void setElementIds()
    {
        this.loginButton.setId("login-button");
        this.userNameTextField.setId("user-name-field");
        this.passwordTextField.setId("password-field");
        this.appCombo.setId("application-field");
    }

    /**
     * Disable enter key listener.
     */
    public void disableListener()
    {
        this.enterKeyListener.disable();
    }

    /**
     * Enable enter key listener.
     */
    public void enableEnterKeyListener()
    {
        this.enterKeyListener.enable();
    }
}
