package fr.hd3d.launcher.ui.client.view.impl;

import com.extjs.gxt.ui.client.binding.FieldBinding;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.widget.form.ComboBox;


/**
 * Binder needed for model data to combo box binding.
 * 
 * @author HD3D
 * 
 * @param <M>
 *            ModelData class
 */
public class ModelComboBoxFieldBinding<M extends ModelData> extends FieldBinding
{
    /** The combo box on which the binding will be set. */
    protected ComboBox<M> comboBox;

    /**
     * Creates a new combo box field binding instance.
     * 
     * @param field
     *            the simple combo box
     * @param property
     *            the property name
     */
    public ModelComboBoxFieldBinding(ComboBox<M> field, String property)
    {
        super(field, property);
        this.comboBox = field;
    }

    @Override
    protected Object onConvertFieldValue(Object value)
    {
        return comboBox.getValue();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Object onConvertModelValue(Object value)
    {
        return comboBox.getStore().findModel((M) value);
    }

}
