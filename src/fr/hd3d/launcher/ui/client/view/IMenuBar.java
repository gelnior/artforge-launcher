package fr.hd3d.launcher.ui.client.view;

import java.util.List;
import java.util.Map;

import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * View to used to display menu bar and its elements.
 * 
 * @author HD3D
 */
public interface IMenuBar
{

    /**
     * Display running application about text with the HD3D symbol.
     * 
     * @param aboutText
     */
    public void displayAbout(String aboutText);

    /**
     * Display a dialog box to ask for confirmation before changing language.
     * 
     * @param selectedLocale
     *            the selected locale
     */
    public void displayConfirmLanguageChange(String selectedLocale);

    /**
     * Display a message box to ask for confirmation before changing application.
     * 
     * @param app
     *            the selected application
     */
    public void displayConfirmAppChange(ApplicationModelData app);

    /** Displays a message box asking for confirmation before logging out. */
    public void displayConfirmLogOut();

    /**
     * Change language by changing URL parameter ?locale=...
     * 
     * @param urlParameters
     *            the parameters to set in the URL.
     * */
    public void changeLanguage(String urlParameters);

    /**
     * Open a new navigator window which go to the URL passed in parameter.
     * 
     * @param url
     *            URL to go to.
     */
    public void openWebsite(String url);

    /** Refresh the application menu. */
    public void refreshAppMenu();

    /** Refresh the user name label. */
    public void refreshUserName();

    /**
     * @return Parameters set in the URL as a map.
     */
    public Map<String, List<String>> getUrlParameterMap();

    /**
     * Open a new browser window with the url set on the launcher.
     * 
     * @param parameters
     *            Parameters to add to url
     */
    public void openLauncher(String parameters, String windowName);
}
