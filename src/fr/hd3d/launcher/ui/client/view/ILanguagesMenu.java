package fr.hd3d.launcher.ui.client.view;

/**
 * Options menu. By default, handles language changing.
 * 
 * @author HD3D
 * 
 */
public interface ILanguagesMenu
{
    /**
     * Check the language given in parameter and uncheck all other language. This action does not raise event.
     * 
     * @param language
     *            the language to check in the language menu.
     */
    public void checkLanguage(String language);

    /**
     * Fill language menu with language stored by the model.
     */
    public void fill();

    /**
     * Return the locale parameter set in the view.
     */
    public String getLocaleParameter();
}
