package fr.hd3d.launcher.ui.client.view;

import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * Simple login form based on GXT widgets including : *
 * <ul>
 * <li>One field for the user name</li>
 * <li>One field for the password</li>
 * <li>One button to submit</li>
 * </ul>
 * 
 * @author HD3D
 */
public interface ILoginDialog
{

    /**
     * @return User name from the text field.
     */
    public String getUserName();

    /**
     * @return password from the the text field.
     */
    public String getPassword();

    /**
     * @return Application selected in the combo box.
     */
    public ApplicationModelData getApplication();

    /**
     * @return true if remember me combo box is checked.
     */
    public boolean getRememberMe();

    /**
     * @return true if all fields are non-empty.
     */
    public boolean isValid();

    /**
     * Reset fields values.
     */
    public void reset();

    /**
     * Select in the combo box the application given in parameter.
     * 
     * @param application
     *            the application to select.
     */
    public void selectApplication(ApplicationModelData application);

    /**
     * @return The application name given in the URL as a parameter.
     */
    public String getApplicationParameter();

    /**
     * @return the cookie containing favorite informations.
     */
    public String getFavCookie();

    /**
     * Show to user that login is waiting for response.
     */
    public void showLoading();

    /**
     * Show to user that loading is finished.
     */
    public void hideLoading();

    /**
     * Hide the combobox Application.
     */
    public void hideApplicationCombobox();

    /**
     * Disable enter key listener.
     */
    public void disableListener();

    /**
     * Enable enter key listener.
     */
    public void enableEnterKeyListener();
}
