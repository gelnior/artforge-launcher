package fr.hd3d.launcher.ui.client.view;

import fr.hd3d.common.ui.client.widget.mainview.IMainView;


/**
 * This view display a dialog which contains a login form. It asks for :
 * <ul>
 * <li>User name</li>
 * <li>Password</li>
 * <li>Application to launch first</li>
 * </ul>
 * 
 * @author HD3D
 */
public interface ILauncher extends IMainView
{

    /** Display login dialog. */
    public void displayLogin();

    /** Display application. */
    public void displayApplication(String appUrl);

    /**
     * Hides frame where application is settled.
     */
    public void hideMainPanel();

    /**
     * If login dialog is visible, it sets the dialog to the center of the navigator window.
     */
    public void centerLoginDialog();

    /** @return locale parameter from navigator URL. */
    public String getLocaleParameter();

    /** @return host HTTP address. */
    public String getHost();

    /** Retrieve authentication cookie set by login web service response processing. */
    public String getAuthCookie();

    /** @return application parameter from navigator URL. */
    public String getApplicationParameter();

    /** Clear all widgets */
    public void clear();

    /**
     * @param key
     *            The key of the value to return.
     * @return the value stored in the favorite cookie corresponding to <i>key</i>.
     */
    public String getFavCookieValue(String key);

    /**
     * Create or update the couple key-value in the favorite cookie.
     * 
     * @param value
     *            the value to set in favorite cookie.
     * @param key
     *            The key of the value to return.
     */
    public void setFavCookieValue(String key, String value);

    /** Delete the favorite cookie. */
    public void removeFavCookie();

    /** Reset loaded application frames. */
    public void resetFrames();
}
