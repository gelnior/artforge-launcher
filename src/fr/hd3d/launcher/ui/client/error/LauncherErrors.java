package fr.hd3d.launcher.ui.client.error;

/**
 * Give a code to all application specific errors raised by the launcher.
 * 
 * @author HD3D
 */
public class LauncherErrors
{
    public static final int APPLICATIONS_LOAD = 11;
    public static final int LANGUAGES_LOAD = 12;
    public static final int INVALID_LOGIN_FORM = 13;
    public static final int INVALID_CREDENTIALS = 14;
    public static final int APPLICATION_EMPTY_CONFIG_FILE = 15;
    public static final int INVALID_SERVER_RESPONSE = 16;
}
