package fr.hd3d.launcher.ui.client.listener;

import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.menu.CheckMenuItem;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.constant.LauncherLanguages;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.model.LanguagesMenuModel;


/**
 * This listener displays a confirmation dialog box before changing language.
 * 
 * @author HD3D
 * 
 * @param <E>
 *            Menu Event
 */
public class ChangeLanguageListener<E extends MenuEvent> extends SelectionListener<E>
{
    /** Model to handle menu bar data */
    private final LanguagesMenuModel model;

    /** Default Constructor */
    public ChangeLanguageListener(LanguagesMenuModel model)
    {
        this.model = model;
    }

    @Override
    public void componentSelected(MenuEvent ce)
    {
        EventDispatcher.forwardEvent(this.makeEvent(ce));
    }

    /**
     * Build event with locale info based on event information
     * 
     * @param ce
     *            Menu Event which contains informations.
     * @return Controller Event with locale parameter
     */
    private AppEvent makeEvent(MenuEvent ce)
    {
        AppEvent event = new AppEvent(LauncherEvents.LANGUAGE_SELECTED);
        event.setData(LauncherConfig.EVENT_VAR_LOCALE, this.getSelectedLocale(ce));
        return event;
    }

    /**
     * Get locale from language selected by the user.
     * 
     * @param ce
     * @return locale selected by the user.
     */
    private String getSelectedLocale(MenuEvent ce)
    {
        CheckMenuItem langItem = (CheckMenuItem) ce.getItem();
        String keyLanguage = LauncherLanguages.getKeyLanguage(langItem.getText());
        return this.model.getLocale(keyLanguage);
    }
}
