package fr.hd3d.launcher.ui.client.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * This listener asks to the controller for displaying a confirmation dialog box before changing application. This is
 * the tool bar version of this event.
 * 
 * @author HD3D
 * 
 */
public class ChangeApplicationToolbarListener extends SelectionListener<ButtonEvent>
{
    /** Model to handle menu bar data */
    private final ApplicationModelData app;

    /** Default Constructor */
    public ChangeApplicationToolbarListener(ApplicationModelData app)
    {
        this.app = app;
    }

    @Override
    public void componentSelected(ButtonEvent ce)
    {
        EventDispatcher.forwardEvent(this.makeEvent(ce));
    }

    /**
     * Build event to dispatch to controllers. The event includes informations about the application to run.
     * 
     * @return event including informations about the application to run.
     */
    private AppEvent makeEvent(ButtonEvent ce)
    {
        AppEvent event = new AppEvent(LauncherEvents.APP_CHANGED);
        event.setData(LauncherConfig.EVENT_VAR_APPLICATION, app);
        return event;
    }
}
