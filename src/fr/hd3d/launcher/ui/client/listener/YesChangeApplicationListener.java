package fr.hd3d.launcher.ui.client.listener;

import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * This listener makes the running application change to the one specified by the user.
 * 
 * @author HD3D
 */
public class YesChangeApplicationListener implements Listener<MessageBoxEvent>
{
    /** Model to handle menu bar data */
    private final ApplicationModelData app;

    /**
     * Default constructor.
     * 
     * @param app
     *            The new application to launch.
     */
    public YesChangeApplicationListener(ApplicationModelData app)
    {
        this.app = app;
    }

    public void handleEvent(MessageBoxEvent be)
    {
        if (this.isYes(be))
        {
            AppEvent event = new AppEvent(LauncherEvents.APP_CHANGED);
            event.setData(LauncherConfig.EVENT_VAR_APPLICATION, this.app);

            EventDispatcher.forwardEvent(event);
        }
    }

    /**
     * @param be
     *            Event from the dialog box.
     * @return true if the yes button was click.
     */
    private boolean isYes(MessageBoxEvent be)
    {
        Dialog dialog = (Dialog) be.getComponent();
        return be.getButtonClicked() == dialog.getButtonById(Dialog.YES);
    }
}
