package fr.hd3d.launcher.ui.client.listener;

import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;


/**
 * When window is resized it forwards a WINDOW_RESIZE event.
 * 
 * @author HD3D
 */
public class LoginWindowResizeListener implements ResizeHandler
{

    public void onResize(ResizeEvent event)
    {
        EventDispatcher.forwardEvent(LauncherEvents.WINDOW_RESIZE);
    }
}
