package fr.hd3d.launcher.ui.client.listener;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.widget.button.Button;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;


/**
 * A key Listener to handle enter key pressing. When key is pressed, the "on mouse down" style is added to the login
 * button. On key up, it launches login clicked event and remove "on mouse down" style from the login button.
 * 
 * @author HD3D
 */
public class LoginDialogKeyListener extends KeyListener
{
    /** Login Button for changing its aspect on enter key pressing. */
    private final Button loginButton;
    /** Base style for GXT button. */
    private final String baseStyle = "x-btn";
    /** True if listener is disabled. */
    private boolean isDisabled = false;
    /**
     * True if login button is pressed. It is useful to know if enter key has been upped inside login form or inside
     * authentication error message. If key has been upped inside error message, the enter key listener should not work.
     */
    private boolean isPressed = false;

    /**
     * Default constructor.
     * 
     * @param loginButton
     *            Login button needed
     */
    public LoginDialogKeyListener(Button loginButton)
    {
        this.loginButton = loginButton;
    }

    /**
     * On key pressed, the "on mouse down" style is set on the login button.
     * 
     * @see com.extjs.gxt.ui.client.event.KeyListener#componentKeyPress(com.extjs.gxt.ui.client.event.ComponentEvent)
     */
    @Override
    public void componentKeyPress(ComponentEvent event)
    {
        if (event.getKeyCode() == 13 && !isDisabled)
        {
            this.loginButton.addStyleName(baseStyle + "-click");
            this.isPressed = true;
        }
    }

    /**
     * On key up, it launches login clicked event and remove "on mouse down" style from the login button.
     * 
     * @see com.extjs.gxt.ui.client.event.KeyListener#componentKeyUp(com.extjs.gxt.ui.client.event.ComponentEvent)
     */
    @Override
    public void componentKeyUp(ComponentEvent event)
    {
        if (event.getKeyCode() == 13 && !isDisabled && this.isPressed)
        {
            EventDispatcher.forwardEvent(LauncherEvents.LOGIN_CLICK);

            this.loginButton.removeStyleName(baseStyle + "-click");
        }
        this.isPressed = false;
    }

    /** Disable listener effect. */
    public void disable()
    {
        this.isDisabled = true;
    }

    public void enable()
    {
        this.isDisabled = false;
    }
}
