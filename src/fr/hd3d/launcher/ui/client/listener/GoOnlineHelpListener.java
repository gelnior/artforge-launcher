package fr.hd3d.launcher.ui.client.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;


/**
 * This listener opens a new navigator window displaying the application online help.
 * 
 * @author HD3D
 */
public class GoOnlineHelpListener extends SelectionListener<ButtonEvent>
{
    @Override
    public void componentSelected(ButtonEvent ce)
    {
        EventDispatcher.forwardEvent(LauncherEvents.GO_ONLINE_HELP_CLICKED);
    }
}
