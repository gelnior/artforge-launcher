package fr.hd3d.launcher.ui.client.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;


/**
 * This listener displays a dialog box with the about message given in </i>config/application.xml</i>.
 * 
 * @author HD3D
 */
public class AboutListener extends SelectionListener<ButtonEvent>
{
    @Override
    public void componentSelected(ButtonEvent ce)
    {
        EventDispatcher.forwardEvent(LauncherEvents.ABOUT_CLICKED);
    }
}
