package fr.hd3d.launcher.ui.client.listener;

import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;


/**
 * This listener opens a new navigator window displaying the selected application. For that it forwards a
 * NEW_APPLICATION_WINDOW event.
 * 
 * @author HD3D
 */
public class NewWindowApplicationListener extends SelectionListener<MenuEvent>
{
    /** The application to display in the new window. */
    private final ApplicationModelData application;

    /**
     * Default constructor.
     * 
     * @param application
     *            The application to display in the new window.
     */
    public NewWindowApplicationListener(ApplicationModelData application)
    {
        this.application = application;
    }

    @Override
    public void componentSelected(MenuEvent ce)
    {
        AppEvent event = new AppEvent(LauncherEvents.NEW_APPLICATION_WINDOW_CLICKED);
        event.setData(LauncherConfig.EVENT_VAR_APPLICATION, application);
        EventDispatcher.forwardEvent(event);
    }
}
