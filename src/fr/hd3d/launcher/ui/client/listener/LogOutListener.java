package fr.hd3d.launcher.ui.client.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;


/**
 * Ask for Confirmation before logging out.
 * 
 * @author HD3D
 */
public class LogOutListener extends SelectionListener<ButtonEvent>
{

    @Override
    public void componentSelected(ButtonEvent ce)
    {
        EventDispatcher.forwardEvent(LauncherEvents.LOG_OUT_CLICKED);
    }
}
