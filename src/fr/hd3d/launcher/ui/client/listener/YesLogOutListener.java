package fr.hd3d.launcher.ui.client.listener;

import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.widget.Dialog;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;


/**
 * This listener makes the navigator go to the login page when log out is confirmed by the user.
 * 
 * @author HD3D
 */
public class YesLogOutListener implements Listener<MessageBoxEvent>
{

    public void handleEvent(MessageBoxEvent be)
    {
        if (this.isYes(be))
        {
            EventDispatcher.forwardEvent(LauncherEvents.LOG_OUT_CONFIRMED);
        }
    }

    /**
     * @param be
     *            Event from the dialog box.
     * @return true if the yes button was click.
     */
    private boolean isYes(MessageBoxEvent be)
    {
        Dialog dialog = (Dialog) be.getComponent();
        return be.getButtonClicked() == dialog.getButtonById(Dialog.YES);
    }
}
