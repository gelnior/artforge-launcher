package fr.hd3d.launcher.ui.client.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;


/**
 * Listener to handle click on the "log in" button of the login dialog.
 * 
 * @author HD3D
 */
public class LoginClickListener extends SelectionListener<ButtonEvent>
{
    @Override
    public void componentSelected(ButtonEvent ce)
    {
        EventDispatcher.forwardEvent(LauncherEvents.LOGIN_CLICK);
    }
}
