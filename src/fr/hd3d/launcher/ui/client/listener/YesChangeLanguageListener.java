package fr.hd3d.launcher.ui.client.listener;

import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;


/**
 * This listener makes the application language change to the one specified by the user.
 * 
 * @author HD3D
 */
public class YesChangeLanguageListener implements Listener<MessageBoxEvent>
{
    /** Model to handle menu bar data */
    private final String selectedLocale;

    /**
     * Default constructor.
     * 
     * @param selectedLocale
     *            The locale to set.
     */
    public YesChangeLanguageListener(String selectedLocale)
    {
        this.selectedLocale = selectedLocale;
    }

    public void handleEvent(MessageBoxEvent be)
    {
        if (this.isYes(be))
        {
            AppEvent event = new AppEvent(LauncherEvents.CHANGE_LANGUAGE_CONFIRMED);
            event.setData(LauncherConfig.EVENT_VAR_LOCALE, this.selectedLocale);

            EventDispatcher.forwardEvent(event);
        }
        else
        {
            EventDispatcher.forwardEvent(LauncherEvents.KEEP_LANGUAGE);
        }
    }

    /**
     * @param be
     *            Event from the dialog box.
     * @return true if the yes button was click.
     */
    private boolean isYes(MessageBoxEvent be)
    {
        Dialog dialog = (Dialog) be.getComponent();
        return be.getButtonClicked() == dialog.getButtonById(Dialog.YES);
    }
}
