package fr.hd3d.launcher.ui.test.mock;

import fr.hd3d.launcher.ui.client.model.LanguagesMenuModel;
import fr.hd3d.launcher.ui.client.view.ILanguagesMenu;


public class LanguagesMenuMock implements ILanguagesMenu
{
    public boolean filled;
    public String checkLanguage;

    /**
     * Default Constructor.
     * 
     * @param model
     *            Data model
     */
    public LanguagesMenuMock(LanguagesMenuModel model)
    {
        this.filled = false;
        this.checkLanguage = new String();
    }

    public void checkLanguage(String language)
    {
        this.checkLanguage = language;
    }

    public void fill()
    {
        this.filled = true;
    }

    public String getLocaleParameter()
    {
        return "en";
    }
}
