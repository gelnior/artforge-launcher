package fr.hd3d.launcher.ui.test.mock;

import org.restlet.client.Response;
import org.restlet.client.Uniform;
import org.restlet.client.data.Form;
import org.restlet.client.data.MediaType;
import org.restlet.client.data.Method;
import org.restlet.client.data.Status;

import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


public class MalformedLoginServiceHandler implements IRestRequestHandler
{
    public final void handleRequest(Method method, final String path, final String jsonObject,
            final BaseCallback callback)
    {
        Response response_restlet = new Response(null);
        response_restlet.setEntity("{\"resulta\":\"ok\",\"userId\":123}", MediaType.APPLICATION_JSON);
        response_restlet.setStatus(Status.SUCCESS_OK);

        callback.handle(null, response_restlet);
    }

    public void handleAttributeRequest(Method method, String path, Form form, BaseCallback callback)
    {
        Response response_restlet = new Response(null);
        response_restlet.setEntity("{\"resulta:\"ok\",\"userId\":123}", MediaType.APPLICATION_JSON);
        response_restlet.setStatus(Status.SUCCESS_OK);

        callback.handle(null, response_restlet);
    }

    public void deleteRequest(String path, BaseCallback callback)
    {}

    public Long getIdFromLocation(String location)
    {
        return null;
    }

    public void getRequest(String path, BaseCallback callback)
    {

    }

    public String getServerUrl()
    {
        return null;
    }

    public String getServicePath()
    {
        return null;
    }

    public String getServicesUrl()
    {
        return null;
    }

    public void postRequest(String path, BaseCallback callback, String jsonObject)
    {}

    public void putRequest(String path, BaseCallback callback, String jsonObject)
    {}

    public void setServerUrl(String serverUrl)
    {}

    public void setServicePath(String servicePath)
    {}

    public void handleRequest(Method method, String path, String jsonObject, Uniform callback)
    {}

}
