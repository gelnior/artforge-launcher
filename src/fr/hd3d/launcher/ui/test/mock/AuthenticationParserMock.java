package fr.hd3d.launcher.ui.test.mock;

import java.io.IOException;
import java.util.Map;

import org.restlet.client.Response;

import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.test.util.modeldata.reader.JSONReaderMock;
import fr.hd3d.launcher.ui.client.model.util.IAuthenticationParser;


/**
 * Authentication query parser for unit tests.
 * 
 * @author HD3D
 */
public class AuthenticationParserMock implements IAuthenticationParser
{
    /** Encoded credentials retrieved from query parsing. */
    String credentials;

    /**
     * @see fr.hd3d.launcher.ui.client.model.util.IAuthenticationParser#analyseAuthenticationResponse(org.restlet.gwt.data.Response)
     */
    public Long analyseAuthenticationResponse(Response response)
    {
        try
        {
            JSONReaderMock reader = new JSONReaderMock();
            Map<?, ?> map = (Map<?, ?>) reader.readString(response.getEntity().getText());

            if (map.get(RESULT) != null && ((String) map.get(RESULT)).compareTo(AUTHENTICATION_SUCCESS_VALUE) == 0)
            {
                return 10L;
            }
            else
            {
                return -1L;
            }
        }
        catch (IOException e)
        {
            GWT.log("Error occured while reading authentication response.", e);
            return -1L;
        }
    }
}
