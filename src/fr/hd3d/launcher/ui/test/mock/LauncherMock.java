package fr.hd3d.launcher.ui.test.mock;

import java.util.HashMap;

import fr.hd3d.launcher.ui.client.view.ILauncher;


public class LauncherMock implements ILauncher
{
    private boolean dialogCentered = false;
    private boolean displayedLoginDialog = false;
    private boolean hiddenAppFrame = false;
    private String authenticationCookie;

    private int errorDisplayed = -1;

    private String applicationDisplayedUrl = new String();
    private final HashMap<String, String> favCookie = new HashMap<String, String>();
    private String applicationParameter = "app_01";
    private boolean isStartLoading = true;
    private boolean isScriptLoading = true;

    public void displayApplication(String appUrl)
    {
        this.applicationDisplayedUrl = appUrl;
    }

    public void displayLogin()
    {
        this.displayedLoginDialog = true;
    }

    public void resetDisplayedLoginDialog()
    {
        this.displayedLoginDialog = false;
    }

    public void hideMainPanel()
    {
        this.hiddenAppFrame = true;
    }

    public String getLocaleParameter()
    {
        return "fr";
    }

    public String getHost()
    {
        return "localhost";
    }

    public void displayError(Integer error)
    {
        this.errorDisplayed = error;
    }

    public void displayError(Integer error, String userMsg, String stack)
    {
        this.errorDisplayed = error;
    }

    public int getDisplayedError()
    {
        return this.errorDisplayed;
    }

    public void centerLoginDialog()
    {
        this.dialogCentered = true;
    }

    public String getApplicationDisplayed()
    {
        return this.applicationDisplayedUrl;
    }

    public boolean getDialogCentered()
    {
        return this.dialogCentered;
    }

    public boolean getDisplayedLoginDialog()
    {
        return this.displayedLoginDialog;
    }

    public boolean getHiddenAppFrame()
    {
        return this.hiddenAppFrame;
    }

    public void resetDisplayedError()
    {
        this.errorDisplayed = -1;
    }

    public String getAuthCookie()
    {
        return this.authenticationCookie;
    }

    public void setAuthCookie(String authenticationCookie)
    {
        this.authenticationCookie = authenticationCookie;
    }

    public void removeAuthCookie()
    {
        this.authenticationCookie = null;
    }

    public String getApplicationParameter()
    {
        return this.applicationParameter;
    }

    public void resetApplicationParameter()
    {
        this.applicationParameter = null;
    }

    public void clear()
    {

    }

    public boolean isStartLoading()
    {
        return this.isStartLoading;
    }

    public void hideStartPanel()
    {
        this.isStartLoading = false;
    }

    public void showStartPanel()
    {
        this.isStartLoading = true;
    }

    public void clearCookie()
    {
        this.favCookie.clear();
    }

    public String getFavCookieValue(String key)
    {
        return this.favCookie.get(key);
    }

    public void setFavCookieValue(String key, String value)
    {
        this.favCookie.put(key, value);
    }

    public void removeFavCookie()
    {

    }

    public void resetFrames()
    {

    }

    public void displayError(Integer error, String stack)
    {
        this.errorDisplayed = error;
    }

    public void hideScriptRunningPanel()
    {
        this.isScriptLoading = false;
    }

    public void showScriptRunningPanel()
    {
        this.isScriptLoading = true;
    }

    public boolean isScriptLoading()
    {
        return isScriptLoading;
    }

}
