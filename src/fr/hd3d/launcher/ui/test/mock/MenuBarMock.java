package fr.hd3d.launcher.ui.test.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.model.MenuBarModel;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.client.view.IMenuBar;


public class MenuBarMock implements IMenuBar
{
    private String confirmationLocale = new String();
    private ApplicationModelData confirmationApplication;
    private boolean confirmationLogout = false;

    private String urlsParameters = new String();

    private boolean isAppMenuRefreshed = false;
    private boolean isUserNameRefreshed = false;

    private String onlineHelpUrl = new String();
    private String aboutText = new String();

    private String newWindowName = new String();
    private String newWindowParameters = new String();

    public MenuBarMock(MenuBarModel model)
    {}

    public ApplicationModelData getConfirmationApplication()
    {
        return this.confirmationApplication;
    }

    public String getConfirmationLocale()
    {
        return this.confirmationLocale;
    }

    public String getUrlsParameters()
    {
        return this.urlsParameters;
    }

    public boolean getConfirmationLogout()
    {
        return this.confirmationLogout;
    }

    public boolean isAppMenuRefreshed()
    {
        return this.isAppMenuRefreshed;
    }

    public boolean isUserNameRefreshed()
    {
        return this.isUserNameRefreshed;
    }

    public String getOnlineHelpUrl()
    {
        return this.onlineHelpUrl;
    }

    public String getAboutText()
    {
        return this.aboutText;
    }

    public String getNewWindowName()
    {
        return this.newWindowName;
    }

    public String getNewWindowParameters()
    {
        return this.newWindowParameters;
    }

    public void changeLanguage(String locale)
    {
        this.urlsParameters = locale;
    }

    public void displayAbout(String aboutText)
    {
        this.aboutText = aboutText;
    }

    public void displayConfirmAppChange(ApplicationModelData app)
    {
        this.confirmationApplication = app;
    }

    public void displayConfirmLanguageChange(String selectedLocale)
    {
        this.confirmationLocale = selectedLocale;
    }

    public void displayConfirmLogOut()
    {
        this.confirmationLogout = true;
    }

    public void openWebsite(String url)
    {
        this.onlineHelpUrl = url;
    }

    public void refreshAppMenu()
    {
        this.isAppMenuRefreshed = true;
    }

    public void refreshUserName()
    {
        this.isUserNameRefreshed = true;
    }

    public void resetRefresh()
    {
        this.isAppMenuRefreshed = false;
        this.isUserNameRefreshed = false;
    }

    public Map<String, List<String>> getUrlParameterMap()
    {
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        List<String> applications = new ArrayList<String>();
        applications.add("application_01");
        List<String> locales = new ArrayList<String>();
        locales.add("fr");
        map.put(LauncherConfig.URL_VAR_APPLICATION, applications);
        map.put(LauncherConfig.URL_VAR_LOCALE, locales);
        return map;
    }

    public void openLauncher(String parameters, String windowName)
    {
        this.newWindowParameters = parameters;
        this.newWindowName = windowName;
    }
}
