package fr.hd3d.launcher.ui.test.mock;

import com.google.gwt.http.client.RequestCallback;

import fr.hd3d.common.ui.client.http.IHttpRequestHandler;


public class HttpRequestHandlerMock implements IHttpRequestHandler
{
    public void getRequest(String url, RequestCallback callback) throws Exception
    {}
}
