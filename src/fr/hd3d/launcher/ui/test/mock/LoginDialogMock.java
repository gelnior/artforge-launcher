package fr.hd3d.launcher.ui.test.mock;

import fr.hd3d.launcher.ui.client.model.LoginModel;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.client.view.ILoginDialog;


public class LoginDialogMock implements ILoginDialog
{
    public static String VALID_LOGIN = "root";
    public static String VALID_PASS = "rootpass";

    private final LoginModel model;

    public boolean reset = false;
    public boolean formValidity = false;
    public boolean errorDisplayed = false;
    private boolean isLoading = false;
    private ApplicationModelData application;
    private String password = new String();
    private String userName = new String();
    private boolean rememberMe = false;

    public LoginDialogMock(LoginModel model)
    {
        this.model = model;
    }

    public ApplicationModelData getApplication()
    {
        return application;
    }

    public String getPassword()
    {
        return this.password;
    }

    public String getUserName()
    {
        return this.userName;
    }

    public boolean getRememberMe()
    {
        return this.rememberMe;
    }

    public void setRememberMe(boolean rememberMe)
    {
        this.rememberMe = rememberMe;
    }

    public void setValidCredentials()
    {
        this.userName = VALID_LOGIN;
        this.password = VALID_PASS;

        this.model.getLoginForm().setUserName(this.userName);
        this.model.getLoginForm().setPassword(this.password);
    }

    public void setWrongCredentials()
    {
        this.userName = "test";
        this.password = "test";
    }

    public boolean isValid()
    {
        return formValidity;
    }

    public void reset()
    {
        this.reset = true;
        this.password = new String();
        this.userName = new String();
        this.application = null;
    }

    public void selectApplication(ApplicationModelData application)
    {
        this.application = application;
        this.model.getLoginForm().setApplication(application);
    }

    public void displayFieldErrorMessage()
    {
        this.errorDisplayed = true;
    }

    public String getApplicationParameter()
    {
        return "application_test";
    }

    public String getFavCookie()
    {
        return "application:gattaca";
    }

    public void setEmptyCredentials()
    {
        this.password = "";
        this.userName = "";
    }

    public void hideLoading()
    {
        this.isLoading = false;
    }

    public void showLoading()
    {
        this.isLoading = true;
    }

    public void hideApplicationCombobox()
    {

    }

    public boolean isLoading()
    {
        return this.isLoading;
    }

    public void disableListener()
    {
    // TODO Auto-generated method stub

    }

    public void enableEnterKeyListener()
    {
    // TODO Auto-generated method stub

    }
}
