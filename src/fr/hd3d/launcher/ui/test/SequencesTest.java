package fr.hd3d.launcher.ui.test;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.http.HttpRequestHandlerSingleton;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.LanguagesMenuController;
import fr.hd3d.launcher.ui.client.controller.LauncherController;
import fr.hd3d.launcher.ui.client.controller.LoginController;
import fr.hd3d.launcher.ui.client.controller.MenuBarController;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.controller.event.LoadApplicationsEvent;
import fr.hd3d.launcher.ui.client.error.LauncherErrors;
import fr.hd3d.launcher.ui.client.model.LanguagesMenuModel;
import fr.hd3d.launcher.ui.client.model.LauncherModel;
import fr.hd3d.launcher.ui.client.model.LoginModel;
import fr.hd3d.launcher.ui.client.model.MenuBarModel;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.client.model.modeldata.LanguageModelData;
import fr.hd3d.launcher.ui.client.model.util.AuthenticationParserSingleton;
import fr.hd3d.launcher.ui.client.model.util.sort.ApplicationAscSortRule;
import fr.hd3d.launcher.ui.test.mock.AuthenticationParserMock;
import fr.hd3d.launcher.ui.test.mock.HttpRequestHandlerMock;
import fr.hd3d.launcher.ui.test.mock.LanguagesMenuMock;
import fr.hd3d.launcher.ui.test.mock.LauncherMock;
import fr.hd3d.launcher.ui.test.mock.LoginDialogMock;
import fr.hd3d.launcher.ui.test.mock.MalformedLoginServiceHandler;
import fr.hd3d.launcher.ui.test.mock.MenuBarMock;


public class SequencesTest extends UITestCase
{
    LauncherModel launcherModel = new LauncherModel();
    LauncherMock launcherView = new LauncherMock();
    LauncherController launcherController = new LauncherController(launcherModel, launcherView);

    LoginModel loginModel = new LoginModel();
    LoginDialogMock loginDialog = new LoginDialogMock(loginModel);
    LoginController loginController = new LoginController(loginModel, loginDialog);

    LanguagesMenuModel languagesMenuModel;
    LanguagesMenuMock languagesMenuView;
    LanguagesMenuController languagesMenuController;

    MenuBarModel menuBarModel = new MenuBarModel();
    MenuBarMock menuBarView = new MenuBarMock(menuBarModel);
    MenuBarController menuBarController = new MenuBarController(menuBarModel, menuBarView);

    ArrayList<LanguageModelData> languages = new ArrayList<LanguageModelData>();
    List<ApplicationModelData> applications = new ArrayList<ApplicationModelData>();

    @Override
    public void setUp()
    {
        super.setUp();

        HttpRequestHandlerSingleton.setInstanceAsMock(new HttpRequestHandlerMock());
        AuthenticationParserSingleton.setInstanceAsMock(new AuthenticationParserMock());

        languagesMenuModel = new LanguagesMenuModel();
        languagesMenuView = new LanguagesMenuMock(languagesMenuModel);
        languagesMenuController = new LanguagesMenuController(languagesMenuView, languagesMenuModel);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(launcherController);

        launcherController.addChild(loginController);
        launcherController.addChild(menuBarController);
        menuBarController.addChild(languagesMenuController);

        this.initData();
    }

    private void initData()
    {
        LanguageModelData english = new LanguageModelData("english", "en");
        LanguageModelData french = new LanguageModelData("french", "fr");
        LanguageModelData spanish = new LanguageModelData("spanish", "sp");
        languages.add(english);
        languages.add(french);
        languages.add(spanish);

        for (int i = 1; i < 15; i++)
        {
            ApplicationModelData application;
            if (i < 10)
            {
                application = new ApplicationModelData("app_0" + i, "family_0" + i % 5, "app_url_0" + i, "help_url_0"
                        + i, "about_text_0" + i);
            }
            else
            {
                application = new ApplicationModelData("app_" + i, "family_" + i % 5, "app_url_" + i, "help_url_" + i,
                        "about_text_" + i);
            }
            applications.add(application);
        }

        this.launcherModel.getApplications().addAll(applications);
        Collections.sort(this.applications, new ApplicationAscSortRule());

        this.languagesMenuModel.setLanguages(languages);
        EventDispatcher.forwardEvent(LauncherEvents.LANGUAGES_DATA_LOADED);
        assertTrue(this.languagesMenuView.filled);
    }

    public void testResize()
    {
        assertFalse(this.launcherView.getDialogCentered());
        EventDispatcher.forwardEvent(LauncherEvents.WINDOW_RESIZE);
        assertTrue(this.launcherView.getDialogCentered());
    }

    public void testEmptyFields()
    {
        loginDialog.setEmptyCredentials();
        EventDispatcher.forwardEvent(LauncherEvents.LOGIN_CLICK);
        assertEquals(LauncherErrors.INVALID_LOGIN_FORM, this.launcherView.getDisplayedError());

        this.launcherView.resetDisplayedError();
        this.loginDialog.setValidCredentials();

        EventDispatcher.forwardEvent(LauncherEvents.LOGIN_CLICK);
        assertEquals(LauncherErrors.INVALID_LOGIN_FORM, this.launcherView.getDisplayedError());
    }

    public void testRightLoginSequence() throws UnsupportedEncodingException
    {
        this.launcherView.displayLogin();
        this.launcherView.resetDisplayedError();
        this.loginDialog.setValidCredentials();
        this.loginDialog.selectApplication(applications.get(2));
        this.launcherView.resetDisplayedError();

        EventDispatcher.forwardEvent(LauncherEvents.LOGIN_CLICK);
        assertEquals(-1, this.launcherView.getDisplayedError());
        assertEquals("app_url_03?locale=en", this.launcherView.getApplicationDisplayed());
        assertNotNull(this.launcherView.getFavCookieValue(LauncherConfig.COOKIE_VAR_APPLICATION));
    }

    public void testMalformedLoginServerResponse()
    {
        this.launcherView.displayLogin();
        this.launcherView.resetDisplayedError();
        this.loginDialog.setValidCredentials();
        this.loginDialog.selectApplication(applications.get(2));
        this.launcherView.resetDisplayedError();

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        RestRequestHandlerSingleton.setInstanceAsMock(new MalformedLoginServiceHandler());
        // Activate it when a better json parser will be set.
        // EventDispatcher.forwardEvent(LauncherEvents.LOGIN_CLICK);
        // assertEquals(LauncherErrors.INVALID_SERVER_RESPONSE, this.launcherView.getDisplayedError());
        RestRequestHandlerSingleton.setInstanceAsMock(requestHandler);
    }

    public void testWrongLoginSequence() throws UnsupportedEncodingException
    {
        this.launcherView.resetDisplayedError();
        this.loginDialog.setWrongCredentials();
        this.loginDialog.selectApplication(applications.get(2));
        EventDispatcher.forwardEvent(LauncherEvents.LOGIN_CLICK);
        assertEquals(LauncherErrors.INVALID_CREDENTIALS, this.launcherView.getDisplayedError());
        assertEquals(0, this.launcherView.getApplicationDisplayed().length());
    }

    public void testLogoutSequence() throws UnsupportedEncodingException
    {
        this.testRightLoginSequence();

        EventDispatcher.forwardEvent(LauncherEvents.LOG_OUT_CLICKED);
        assertTrue(this.menuBarView.getConfirmationLogout());

        EventDispatcher.forwardEvent(LauncherEvents.LOG_OUT);
        assertTrue(this.launcherView.getDisplayedLoginDialog());
        assertTrue(this.launcherView.getHiddenAppFrame());
        assertEquals(0, this.loginDialog.getPassword().length());
        assertEquals(0, this.loginDialog.getUserName().length());
        assertNull(this.loginDialog.getApplication());

        LoadApplicationsEvent event = new LoadApplicationsEvent();
        event.setApplications(new ArrayList<ApplicationModelData>());
        EventDispatcher.forwardEvent(event);
        assertTrue(this.launcherView.getDisplayedLoginDialog());
        assertNull(this.launcherView.getAuthCookie());
        assertNotNull(this.launcherView.getFavCookieValue(LauncherConfig.COOKIE_VAR_APPLICATION));
    }

    public void testApplicationAboutSequence() throws UnsupportedEncodingException
    {
        this.testRightLoginSequence();
        EventDispatcher.forwardEvent(LauncherEvents.ABOUT_CLICKED);
        assertEquals("about_text_03", this.menuBarView.getAboutText());
    }

    public void testHelpSequence() throws UnsupportedEncodingException
    {
        this.testRightLoginSequence();
        EventDispatcher.forwardEvent(LauncherEvents.GO_ONLINE_HELP_CLICKED);
        assertEquals("help_url_03", this.menuBarView.getOnlineHelpUrl());
    }

    public void testChangeLanguageSequence() throws UnsupportedEncodingException
    {
        this.testRightLoginSequence();
        assertEquals("en", this.launcherModel.getLocale());

        AppEvent event = new AppEvent(LauncherEvents.LANGUAGE_SELECTED);
        event.setData(LauncherConfig.EVENT_VAR_LOCALE, "fr");

        EventDispatcher.forwardEvent(event);
        assertEquals("fr", this.menuBarView.getConfirmationLocale());

        EventDispatcher.forwardEvent(LauncherEvents.KEEP_LANGUAGE);
        assertEquals(this.languagesMenuView.checkLanguage, this.languagesMenuModel.getLanguage());

        EventDispatcher.forwardEvent(event);
        assertEquals("fr", this.menuBarView.getConfirmationLocale());

        assertEquals("english", this.languagesMenuModel.getLanguage());
        event = new AppEvent(LauncherEvents.CHANGE_LANGUAGE_CONFIRMED);
        event.setData(LauncherConfig.EVENT_VAR_LOCALE, "fr");
        EventDispatcher.forwardEvent(event);

        assertEquals("fr", this.languagesMenuModel.getLocale());
        assertEquals("french", this.languagesMenuModel.getLanguage());
        boolean parametersTest = this.menuBarView.getUrlsParameters().equals("application=application_01&locale=fr")
                || this.menuBarView.getUrlsParameters().equals("locale=fr&application=application_01");
        assertTrue(parametersTest);

        event = new AppEvent(LauncherEvents.LANGUAGE_SELECTED);
        event.setData(LauncherConfig.EVENT_VAR_LOCALE, "sp");

        EventDispatcher.forwardEvent(event);
        assertEquals("sp", this.menuBarView.getConfirmationLocale());

        EventDispatcher.forwardEvent(LauncherEvents.KEEP_LANGUAGE);
        assertEquals(this.languagesMenuView.checkLanguage, this.languagesMenuModel.getLanguage());
        assertEquals("fr", this.languagesMenuModel.getLocale());

        EventDispatcher.forwardEvent(event);
        assertEquals("sp", this.menuBarView.getConfirmationLocale());

        event = new AppEvent(LauncherEvents.CHANGE_LANGUAGE_CONFIRMED);
        event.setData(LauncherConfig.EVENT_VAR_LOCALE, "sp");
        EventDispatcher.forwardEvent(event);

        assertEquals("sp", this.languagesMenuModel.getLocale());
        assertEquals("spanish", this.languagesMenuModel.getLanguage());
        assertEquals("application=application_01&locale=sp", this.menuBarView.getUrlsParameters());
        parametersTest = this.menuBarView.getUrlsParameters().equals("application=application_01&locale=sp")
                || this.menuBarView.getUrlsParameters().equals("locale=sp&application=application_01");
        assertTrue(parametersTest);
    }

    public void testChangeApplication() throws UnsupportedEncodingException
    {
        this.launcherView.resetApplicationParameter();
        this.testRightLoginSequence();

        assertEquals(this.launcherModel.getApplications().get(2).getName().toLowerCase(),
                this.launcherView.getFavCookieValue(LauncherConfig.COOKIE_VAR_APPLICATION));
        assertEquals(this.launcherModel.getLocale(),
                this.launcherView.getFavCookieValue(LauncherConfig.COOKIE_VAR_LOCALE));
        assertEquals(false, this.launcherView.isStartLoading());

        this.menuBarView.resetRefresh();
        ApplicationModelData application = applications.get(6);
        AppEvent event = new AppEvent(LauncherEvents.APP_CHANGING_CONFIRMATION);
        event.setData(LauncherConfig.EVENT_VAR_APPLICATION, application);

        EventDispatcher.forwardEvent(event);
        assertEquals(application, this.menuBarView.getConfirmationApplication());
        assertEquals("app_03", this.menuBarModel.getMainApp().getName());
        assertFalse(this.menuBarView.isAppMenuRefreshed());
        assertFalse(this.menuBarView.isUserNameRefreshed());

        event = new AppEvent(LauncherEvents.APP_CHANGED);
        event.setData(LauncherConfig.EVENT_VAR_APPLICATION, application);

        EventDispatcher.forwardEvent(event);
        assertEquals(application, this.menuBarView.getConfirmationApplication());
        assertEquals("app_07", this.menuBarModel.getMainApp().getName());

        for (ApplicationModelData family_application : this.menuBarModel.getFamilyApps())
        {
            assertEquals(application.getFamily(), family_application.getFamily());
        }

        for (ApplicationModelData foreign_application : this.menuBarModel.getForeignApps())
        {
            assertNotSame(application.getFamily(), foreign_application.getFamily());
        }

        assertEquals(application.getName().toLowerCase(), this.launcherView.getFavCookieValue("application"));
        assertEquals(this.launcherModel.getLocale(),
                this.launcherView.getFavCookieValue(LauncherConfig.COOKIE_VAR_LOCALE));
        ;

        this.launcherView.resetDisplayedLoginDialog();
        this.launcherView.setAuthCookie("authentified");
        LoadApplicationsEvent eventReload = new LoadApplicationsEvent();
        eventReload.setApplications(this.launcherModel.getApplications());
        EventDispatcher.forwardEvent(eventReload);

        assertFalse(this.launcherView.getDisplayedLoginDialog());
        assertEquals(this.launcherModel.getApplications().get(6).getName(), this.launcherController.selectApplication()
                .getName());
        assertEquals(this.launcherModel.getApplications().get(6).getName(), this.menuBarModel.getMainApp().getName());
    }

    public void testNoConnectionToServer()
    {
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.setServerUrl("http://wrong_server/");

        this.launcherView.resetDisplayedError();
        this.loginDialog.setValidCredentials();
        this.loginDialog.selectApplication(applications.get(2));
        this.launcherView.resetDisplayedError();

        EventDispatcher.forwardEvent(LauncherEvents.LOGIN_CLICK);
    }
}
