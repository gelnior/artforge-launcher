package fr.hd3d.launcher.ui.test;

import java.util.ArrayList;

import junit.framework.TestCase;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.MenuBarController;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.controller.event.LoadApplicationsEvent;
import fr.hd3d.launcher.ui.client.model.LoginForm;
import fr.hd3d.launcher.ui.client.model.MenuBarModel;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.test.mock.MenuBarMock;


public class MenuBarControllerTest extends TestCase
{
    MenuBarModel model = new MenuBarModel();
    MenuBarMock view = new MenuBarMock(model);
    MenuBarController controller = new MenuBarController(model, view);

    @Override
    public void setUp()
    {
        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    public void testApplicationsDataLoaded()
    {
        ArrayList<ApplicationModelData> applications = new ArrayList<ApplicationModelData>();
        ApplicationModelData application_01 = new ApplicationModelData();
        ApplicationModelData application_02 = new ApplicationModelData();
        ApplicationModelData application_03 = new ApplicationModelData();
        applications.add(application_01);
        applications.add(application_02);
        applications.add(application_03);

        LoadApplicationsEvent event = new LoadApplicationsEvent();
        event.setData(LauncherConfig.APPLICATIONS_EVENT_VAR_NAME, applications);

        EventDispatcher.forwardEvent(event);
        assertEquals(3, this.model.getApplications().size());
    }

    public void testAlreadyLogged()
    {
        LoginForm loginForm = new LoginForm("user", "pass", new ApplicationModelData(), false);

        AppEvent event = new AppEvent(LauncherEvents.ALREADY_LOGGED);
        event.setData(LauncherConfig.EVENT_VAR_LOGIN_FORM, loginForm);
        EventDispatcher.forwardEvent(event);

        assertEquals(loginForm.getUserName(), this.model.getUserName());
        assertEquals(loginForm.getApplication(), this.model.getMainApp());
    }

    public void testLogin()
    {
        LoginForm loginForm = new LoginForm("user", "pass", new ApplicationModelData(), false);

        AppEvent event = new AppEvent(LauncherEvents.IS_LOGGING);
        event.setData(LauncherConfig.EVENT_VAR_LOGIN_FORM, loginForm);
        EventDispatcher.forwardEvent(event);

        assertTrue(this.view.isUserNameRefreshed());
        assertTrue(this.view.isAppMenuRefreshed());
    }

    public void testConfirmLogOut()
    {
        EventDispatcher.forwardEvent(LauncherEvents.LOG_OUT_CLICKED);
        assertTrue(this.view.getConfirmationLogout());
    }

    public void testLaunch()
    {
        EventDispatcher.forwardEvent(LauncherEvents.LAUNCHING);
        assertTrue(this.view.isAppMenuRefreshed());
        assertTrue(this.view.isUserNameRefreshed());
    }

    public void testAbout()
    {
        ApplicationModelData application = new ApplicationModelData();
        application.setAboutText("about_test_text");
        this.model.setMainApp(application);

        EventDispatcher.forwardEvent(LauncherEvents.ABOUT_CLICKED);
        assertEquals(application.getAboutText(), this.view.getAboutText());
    }

    public void testGoOnlineHelp()
    {
        ApplicationModelData application = new ApplicationModelData();
        application.setUrl("http://test.help.com/");
        this.model.setMainApp(application);

        EventDispatcher.forwardEvent(LauncherEvents.GO_ONLINE_HELP_CLICKED);
        assertEquals(application.getHelpUrl(), this.view.getOnlineHelpUrl());
    }

    public void testConfirmChangeApp()
    {
        ApplicationModelData application = new ApplicationModelData();
        AppEvent event = new AppEvent(LauncherEvents.APP_CHANGING_CONFIRMATION);
        event.setData(LauncherConfig.EVENT_VAR_APPLICATION, application);

        EventDispatcher.forwardEvent(event);
        assertEquals(application, this.view.getConfirmationApplication());
        assertFalse(this.view.isAppMenuRefreshed());
        assertFalse(this.view.isUserNameRefreshed());
    }

    public void testChangeApp()
    {
        ApplicationModelData application = new ApplicationModelData();
        AppEvent event = new AppEvent(LauncherEvents.APP_CHANGED);
        event.setData(LauncherConfig.EVENT_VAR_APPLICATION, application);

        EventDispatcher.forwardEvent(event);
        assertEquals(application, this.model.getMainApp());
    }

    public void testConfirmChangeLanguage()
    {
        AppEvent event = new AppEvent(LauncherEvents.CHANGE_LANGUAGE_CONFIRMED);
        event.setData(LauncherConfig.EVENT_VAR_LOCALE, "fr");

        ApplicationModelData application_01 = new ApplicationModelData();
        application_01.setName("application_01");
        this.model.setMainApp(application_01);
        EventDispatcher.forwardEvent(event);

        boolean parametersTest = this.view.getUrlsParameters().equals("application=application_01&locale=fr")
                || this.view.getUrlsParameters().equals("locale=fr&application=application_01");
        assertTrue(parametersTest);
    }

    public void testChangeLanguage()
    {
        AppEvent event = new AppEvent(LauncherEvents.LANGUAGE_SELECTED);
        event.setData(LauncherConfig.EVENT_VAR_LOCALE, "fr");

        EventDispatcher.forwardEvent(event);
        assertEquals("fr", this.view.getConfirmationLocale());
    }

    public void testOnNewApplicationWindow()
    {
        ApplicationModelData application = new ApplicationModelData("application_01", null, null, null, null);
        AppEvent event = new AppEvent(LauncherEvents.NEW_APPLICATION_WINDOW_CLICKED);
        event.setData(LauncherConfig.EVENT_VAR_APPLICATION, application);
        EventDispatcher.forwardEvent(event);

        assertEquals("Window_application_01", this.view.getNewWindowName());
        boolean parametersTest = this.view.getNewWindowParameters().equals("application=application_01&locale=fr")
                || this.view.getNewWindowParameters().equals("locale=fr&application=application_01");
        assertTrue(parametersTest);
    }
}
