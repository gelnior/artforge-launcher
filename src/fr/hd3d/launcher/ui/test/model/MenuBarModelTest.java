package fr.hd3d.launcher.ui.test.model;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.launcher.ui.client.controller.MenuBarController;
import fr.hd3d.launcher.ui.client.model.MenuBarModel;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.test.mock.MenuBarMock;


public class MenuBarModelTest extends TestCase
{
    MenuBarModel model = new MenuBarModel();
    List<ApplicationModelData> applications = new ArrayList<ApplicationModelData>();
    MenuBarMock view = new MenuBarMock(model);
    MenuBarController controller = new MenuBarController(model, view);

    @Override
    public void setUp()
    {
        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    public void testOrderApps()
    {
        for (int i = 1; i < 15; i++)
        {
            ApplicationModelData app;
            if (i < 10)
            {
                app = new ApplicationModelData("app_0" + i, "family_0" + i % 5, "app_url_0" + i, "help_url_0" + i,
                        "about_text_0" + i);
            }
            else
            {
                app = new ApplicationModelData("app_" + i, "family_" + i % 5, "app_url_" + i, "help_url_" + i,
                        "about_text_" + i);
            }

            applications.add(app);
        }
        this.model.setApplications(applications);

        ApplicationModelData application = applications.get(0);
        this.model.setMainApp(application);
        assertApplicationFamily(application);

        ApplicationModelData application_02 = applications.get(13);
        this.model.setMainApp(application_02);
        assertApplicationFamily(application_02);

        ApplicationModelData application_03 = applications.get(7);
        this.model.setMainApp(application_03);
        assertApplicationFamily(application_03);
    }

    public void testOrderOneFamily()
    {
        for (int i = 1; i < 15; i++)
        {
            ApplicationModelData app;
            if (i < 10)
            {
                app = new ApplicationModelData("app_0" + i, "family", "app_url_0" + i, "help_url_0" + i, "about_text_0"
                        + i);
            }
            else
            {
                app = new ApplicationModelData("app_" + i, "family", "app_url_" + i, "help_url_" + i, "about_text_" + i);
            }

            applications.add(app);
        }
        this.model.setApplications(applications);
        assertEquals(0, this.model.getForeignApps().size());

        ApplicationModelData application = applications.get(0);
        this.model.setMainApp(application);
        assertApplicationFamily(application);

        ApplicationModelData application_02 = applications.get(13);
        this.model.setMainApp(application_02);
        assertApplicationFamily(application_02);

        ApplicationModelData application_03 = applications.get(7);
        this.model.setMainApp(application_03);
        assertApplicationFamily(application_03);
    }

    public void assertApplicationFamily(ApplicationModelData application)
    {
        for (ApplicationModelData family_application : this.model.getFamilyApps())
        {
            assertEquals(application.getFamily(), family_application.getFamily());
        }

        for (ApplicationModelData foreign_application : this.model.getForeignApps())
        {
            assertNotSame(application.getFamily(), foreign_application.getFamily());
        }

        assertEquals(this.applications.size(), this.model.getFamilyApps().size() + this.model.getForeignApps().size());
    }
}
