package fr.hd3d.launcher.ui.test.model;

import java.util.ArrayList;

import junit.framework.TestCase;
import fr.hd3d.common.ui.client.http.HttpRequestHandlerSingleton;
import fr.hd3d.launcher.ui.client.model.LanguagesMenuModel;
import fr.hd3d.launcher.ui.client.model.modeldata.LanguageModelData;
import fr.hd3d.launcher.ui.test.mock.HttpRequestHandlerMock;


public class LanguagesMenuModelTest extends TestCase
{
    LanguagesMenuModel model;
    ArrayList<LanguageModelData> languages = new ArrayList<LanguageModelData>();

    @Override
    public void setUp()
    {
        HttpRequestHandlerSingleton.setInstanceAsMock(new HttpRequestHandlerMock());
        model = new LanguagesMenuModel();
        LanguageModelData english = new LanguageModelData("english", "en");
        LanguageModelData french = new LanguageModelData("french", "fr");
        LanguageModelData spanish = new LanguageModelData("spanish", "sp");
        languages.add(english);
        languages.add(french);
        languages.add(spanish);

        this.model.setLanguages(languages);
    }

    public void testGetLocale()
    {
        assertEquals("en", this.model.getLocale("english"));
        assertEquals("fr", this.model.getLocale("french"));
        assertEquals("sp", this.model.getLocale("spanish"));
        assertEquals(null, this.model.getLocale("german"));

        LanguageModelData german = new LanguageModelData("german", "de");
        languages.add(german);
        this.model.setLanguages(languages);
        assertEquals("de", this.model.getLocale("german"));

        try
        {
            assertEquals(null, this.model.getLocale(null));
        }
        catch (NullPointerException e)
        {
            assertTrue(true);
        }
    }

    public void testGetLanguage()
    {
        assertEquals("english", this.model.getLanguage());

        this.model.setLocale("fr");
        assertEquals("french", this.model.getLanguage());

        this.model.setLocale("sp");
        assertEquals("spanish", this.model.getLanguage());

        this.model.setLocale("ck");
        assertEquals(null, this.model.getLanguage());

        this.model.setLocale(null);
        assertEquals("english", this.model.getLanguage());

        this.model.getLanguages().clear();
        assertEquals(null, this.model.getLanguage());
    }
}
