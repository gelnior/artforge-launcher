package fr.hd3d.launcher.ui.test;

import junit.framework.TestCase;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.http.HttpRequestHandlerSingleton;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.LauncherController;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.error.LauncherErrors;
import fr.hd3d.launcher.ui.client.model.LauncherModel;
import fr.hd3d.launcher.ui.client.model.LoginForm;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.test.mock.HttpRequestHandlerMock;
import fr.hd3d.launcher.ui.test.mock.LauncherMock;


public class LauncherControllerTest extends TestCase
{
    LauncherModel model;
    LauncherMock view;
    LauncherController controller;

    @Override
    public void setUp()
    {
        HttpRequestHandlerSingleton.setInstanceAsMock(new HttpRequestHandlerMock());
        model = new LauncherModel();
        view = new LauncherMock();
        controller = new LauncherController(model, view);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    public void testInit()
    {

    }

    public void testApplicationsLoaded()
    {
        ApplicationModelData application = new ApplicationModelData();
        assertEquals(true, this.view.isStartLoading());
        EventDispatcher.forwardEvent(LauncherEvents.APPLICATIONS_DATA_LOADED);
        assertEquals(LauncherErrors.APPLICATION_EMPTY_CONFIG_FILE, this.view.getDisplayedError());

        this.view.resetDisplayedError();
        this.model.getApplications().add(application);
        EventDispatcher.forwardEvent(LauncherEvents.APPLICATIONS_DATA_LOADED);
        assertEquals(-1, this.view.getDisplayedError());
        assertEquals(true, this.view.getDisplayedLoginDialog());

        this.view.resetDisplayedLoginDialog();
        this.view.setAuthCookie("user:pass");
        EventDispatcher.forwardEvent(LauncherEvents.APPLICATIONS_DATA_LOADED);
        assertFalse(this.view.getDisplayedLoginDialog());
        assertFalse(this.view.isStartLoading());
    }

    public void testLogin()
    {
        LoginForm loginForm = new LoginForm("user", "pass", new ApplicationModelData(), false);
        loginForm.setUserId(123L);
        assertEquals(true, this.view.isStartLoading());
        AppEvent event = new AppEvent(LauncherEvents.IS_LOGGING);
        event.setData(LauncherConfig.EVENT_VAR_LOGIN_FORM, loginForm);
        EventDispatcher.forwardEvent(event);

        assertEquals(loginForm.getUserName(), this.view.getFavCookieValue(LauncherConfig.COOKIE_VAR_USER));
    }

    public void testSelectApplication()
    {
        ApplicationModelData application_01 = new ApplicationModelData();
        application_01.setName("app_01");
        ApplicationModelData application_02 = new ApplicationModelData();
        application_02.setName("app_02");
        this.model.getApplications().add(application_01);
        this.model.getApplications().add(application_02);

        assertEquals(this.view.getApplicationParameter(), this.controller.selectApplication().getName());

        this.view.resetApplicationParameter();
        assertEquals(this.model.getApplications().get(0).getName(), this.controller.selectApplication().getName());

        this.view.clearCookie();
        this.view.setFavCookieValue("application", "app_02");

        assertEquals(application_02.getName(), this.controller.selectApplication().getName());
    }

    public void testLaunch()
    {
        ApplicationModelData application = new ApplicationModelData();
        application.setUrl("gattaca");
        application.setName("Gattaca");

        AppEvent event = new AppEvent(LauncherEvents.LAUNCHING);
        event.setData(LauncherConfig.EVENT_VAR_APPLICATION, application);

        model.initLocale(null);
        EventDispatcher.forwardEvent(event);
        assertEquals("gattaca?locale=en", this.view.getApplicationDisplayed());

        assertEquals(application.getName().toLowerCase(),
                this.view.getFavCookieValue(LauncherConfig.COOKIE_VAR_APPLICATION));
        assertEquals(this.model.getLocale(), this.view.getFavCookieValue(LauncherConfig.COOKIE_VAR_LOCALE));
        assertEquals(false, this.view.isStartLoading());
    }

    public void testLogout()
    {
        assertFalse(this.view.getDisplayedLoginDialog());
        assertFalse(this.view.getHiddenAppFrame());
        EventDispatcher.forwardEvent(LauncherEvents.LOG_OUT);

        assertTrue(this.view.getDisplayedLoginDialog());
        assertTrue(this.view.getHiddenAppFrame());
    }

    public void testWindowResize()
    {
        assertFalse(this.view.getDialogCentered());
        EventDispatcher.forwardEvent(LauncherEvents.WINDOW_RESIZE);
        assertTrue(this.view.getDialogCentered());
    }

    public void testDisplayError()
    {
        assertEquals(-1, this.view.getDisplayedError());
        ErrorDispatcher.sendError(CommonErrors.BAD_CONFIG_FILE);
        assertEquals(CommonErrors.BAD_CONFIG_FILE, this.view.getDisplayedError());

        ErrorDispatcher.sendError(LauncherErrors.INVALID_LOGIN_FORM);
        assertEquals(LauncherErrors.INVALID_LOGIN_FORM, this.view.getDisplayedError());
    }
}
