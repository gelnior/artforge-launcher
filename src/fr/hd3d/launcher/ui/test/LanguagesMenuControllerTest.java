package fr.hd3d.launcher.ui.test;

import java.util.ArrayList;

import junit.framework.TestCase;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.http.HttpRequestHandlerSingleton;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.LanguagesMenuController;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.model.LanguagesMenuModel;
import fr.hd3d.launcher.ui.client.model.modeldata.LanguageModelData;
import fr.hd3d.launcher.ui.test.mock.HttpRequestHandlerMock;
import fr.hd3d.launcher.ui.test.mock.LanguagesMenuMock;


public class LanguagesMenuControllerTest extends TestCase
{
    LanguagesMenuModel model;
    LanguagesMenuMock view;
    LanguagesMenuController controller;

    @Override
    public void setUp()
    {
        HttpRequestHandlerSingleton.setInstanceAsMock(new HttpRequestHandlerMock());

        model = new LanguagesMenuModel();
        view = new LanguagesMenuMock(model);
        controller = new LanguagesMenuController(view, model);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);

        this.model.setLocale("en");
        LanguageModelData english = new LanguageModelData("english", "en");
        LanguageModelData french = new LanguageModelData("french", "fr");
        ArrayList<LanguageModelData> languages = new ArrayList<LanguageModelData>();
        languages.add(english);
        languages.add(french);

        this.model.setLanguages(languages);
    }

    public void testInit()
    {
        EventDispatcher.forwardEvent(CommonEvents.CONFIG_INITIALIZED);
        assertEquals(0, this.model.getLocale().compareTo("en"));
    }

    public void testChangeLanguage()
    {
        AppEvent event = new AppEvent(LauncherEvents.CHANGE_LANGUAGE_CONFIRMED);
        event.setData(LauncherConfig.EVENT_VAR_LOCALE, "fr");
        EventDispatcher.forwardEvent(event);

        assertEquals("fr", this.model.getLocale());
        assertEquals("french", this.model.getLanguage());

        event.setData(LauncherConfig.EVENT_VAR_LOCALE, "en");
        EventDispatcher.forwardEvent(event);

        assertEquals("en", this.model.getLocale());
        assertEquals("english", this.model.getLanguage());

        event.setData(LauncherConfig.EVENT_VAR_LOCALE, "it");
        EventDispatcher.forwardEvent(event);

        assertEquals("it", this.model.getLocale());
        assertEquals(null, this.model.getLanguage());
    }

    public void testKeepLanguage()
    {
        AppEvent event = new AppEvent(LauncherEvents.CHANGE_LANGUAGE_CONFIRMED);
        event.setData(LauncherConfig.EVENT_VAR_LOCALE, "fr");
        EventDispatcher.forwardEvent(event);

        assertEquals("fr", this.model.getLocale());
        assertEquals("french", this.model.getLanguage());

        this.view.checkLanguage("english");
        assertNotSame(this.view.checkLanguage, this.model.getLanguage());

        EventDispatcher.forwardEvent(LauncherEvents.KEEP_LANGUAGE);
        assertEquals(this.view.checkLanguage, this.model.getLanguage());
    }

    public void testLanguagesDataLoaded()
    {
        EventDispatcher.forwardEvent(LauncherEvents.LANGUAGES_DATA_LOADED);
        assertTrue(this.view.filled);
    }
}
