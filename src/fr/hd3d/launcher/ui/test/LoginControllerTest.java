package fr.hd3d.launcher.ui.test;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.launcher.ui.client.config.LauncherConfig;
import fr.hd3d.launcher.ui.client.controller.LoginController;
import fr.hd3d.launcher.ui.client.controller.event.LauncherEvents;
import fr.hd3d.launcher.ui.client.controller.event.LoadApplicationsEvent;
import fr.hd3d.launcher.ui.client.model.LoginModel;
import fr.hd3d.launcher.ui.client.model.modeldata.ApplicationModelData;
import fr.hd3d.launcher.ui.client.model.util.AuthenticationParserSingleton;
import fr.hd3d.launcher.ui.test.mock.AuthenticationParserMock;
import fr.hd3d.launcher.ui.test.mock.LoginDialogMock;


public class LoginControllerTest extends UITestCase
{
    LoginModel model;
    LoginDialogMock view;
    LoginController controller;

    @Override
    public void setUp()
    {
        super.setUp();

        AuthenticationParserSingleton.setInstanceAsMock(new AuthenticationParserMock());

        EventDispatcher dispatcher = EventDispatcher.get();
        model = new LoginModel();
        view = new LoginDialogMock(model);
        controller = new LoginController(model, view);

        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    public void testLogOutConfirmed()
    {
        assertFalse(this.view.reset);
        EventDispatcher.forwardEvent(LauncherEvents.LOG_OUT_CONFIRMED);
        assertTrue(this.view.reset);
    }

    public void testLogOut()
    {
        assertFalse(this.view.reset);
        EventDispatcher.forwardEvent(LauncherEvents.LOG_OUT);
        assertTrue(this.view.reset);
    }

    public void testApplicationsLoaded()
    {
        ApplicationModelData app_01 = new ApplicationModelData();
        ApplicationModelData app_02 = new ApplicationModelData();
        ApplicationModelData app_03 = new ApplicationModelData();
        List<ApplicationModelData> applications = new ArrayList<ApplicationModelData>();

        app_02.setName("application_test");

        applications.add(app_01);
        applications.add(app_02);
        applications.add(app_03);

        LoadApplicationsEvent event = new LoadApplicationsEvent();
        event.setData(LauncherConfig.APPLICATIONS_EVENT_VAR_NAME, applications);
        EventDispatcher.forwardEvent(event);

        assertEquals(3, this.model.getComboStore().getCount());
        assertEquals("application_test", this.view.getApplication().getName());
    }

    public void testLoginClick()
    {
        ApplicationModelData application = new ApplicationModelData("Gattaca", "Asset", "/gattaca", "/help", "About");
        this.view.selectApplication(application);
        this.view.setValidCredentials();

        EventDispatcher.forwardEvent(LauncherEvents.LOGIN_CLICK);
        assertFalse(this.view.isLoading());
    }

    public void testLogin()
    {
        this.view.showLoading();
        EventDispatcher.forwardEvent(LauncherEvents.IS_LOGGING);
        assertFalse(this.view.isLoading());
    }

    public void testError()
    {
        this.view.showLoading();
        EventDispatcher.forwardEvent(CommonEvents.ERROR);
        assertFalse(this.view.isLoading());
    }
}
